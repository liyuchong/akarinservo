/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"
#include "self_test/self_test.h"
#include "utils/cJSON.h"
#include "command/command.h"

#include "utils/log.h"

#include "script.h"

#include "kinematics/delta_kinematics.h"

uint32_t global_error = 0;

void status_led_process(void)
{
	if(global_error > 0)
	{
		return;
	}

	if(mc_session.led_tick_count > 10000)
    {
        LED_GREEN_HIGH();
        LED_BLUE_HIGH();
        LED_RED_HIGH();

        if(mc_session.led_tick_count > 20000)
        {
            mc_session.led_tick_count = 0;
        }
    }
    else
    {
        LED_GREEN_LOW();
        LED_BLUE_LOW();
        LED_RED_LOW();
    }
}

extern int initialize_kvmap(void);

static void hardware_init(void)
{
    dsp_uart_setup();
    dsp_pwm_setup();
    dsp_spi_setup();
    dai_spi_setup();
    dsp_gpio_setup();

    controller_eeprom_driver_init();
}

static void encoder_detect(void)
{
	int encoder_not_found = 0;

    /* detect if the encoder is installed */
    if(mag_encoder_detect() != 1)
    {
        SET_ERROR(ERROR_ENCDR_MISSING);

        LED_RED_LOW();
        LED_BLUE_HIGH();

        encoder_not_found = 1;
    }

    /* do not enable control loop if encoder is not detected */
    if(!encoder_not_found)
    {
    	core_control_loop_disable();
    	motor_control_initialize_components();

        mc_session.state = CONTROL_POSITION;

        core_control_loop_enable();
    }
    else
    {
        core_control_loop_enabled = 0;
    }
}

extern int seconds;
uint64_t last_tick = 0;

void tick_second(void)
{
	seconds++;

	mc_session.performance.pos_loop_freq = mc_session.performance.pos_loop_tick;
	mc_session.performance.pos_loop_tick = 0;

	mc_session.performance.vel_loop_freq = mc_session.performance.vel_loop_tick;
	mc_session.performance.vel_loop_tick = 0;
}

void tick_second_process(void)
{
	if(last_tick != mc_session.tick && mc_session.tick % CONTROL_FREQUENCY == 0)
	{
		tick_second();
	}

	last_tick = mc_session.tick;
}

void encoder_setup(void)
{
    mc_session.encoder_info.rev_pulse = 16384;
    mc_session.encoder_info.half_rev_pulse = mc_session.encoder_info.rev_pulse / 2;
    mc_session.encoder_info.deg_per_pulse = 360.0f / (float)mc_session.encoder_info.rev_pulse;
    mc_session.encoder_info.encoder_ratio = 16384.0f / (float)mc_session.encoder_info.rev_pulse;
    lprintf("encoder setup: rev_pulse: %d\r\n",
    		mc_session.encoder_info.rev_pulse);
}

int log_info(void)
{
	lprintf(AKARIN_VER_STRING "\r\n");

	lprintf("control loop: %dHz\r\n", CONTROL_FREQUENCY);
	lprintf("FFT enabled: %d\r\n", VIBS_FFT_ENABLED);
	lprintf("anti-cogging enabled: %d\r\n", ANTI_COGGING);
	lprintf("PDFF for vel loop: %d\r\n", VEL_LOOP_USE_PDFF);

	lprintf("scope channels: %d\r\n", VAR_SCOPE_MAX_COLUMNS);
	lprintf("scope max samples: %d\r\n", VAR_SCOPE_MAX_LENGTH);

	return 0;
}

#pragma optimize_off
int main(int argc, char *argv[])
{
    adi_initComponents();

    dsp_pll_setup();
    hardware_init();

    /* initialize logging system */
    log_init();
    log_info();

    /* disable UART output during setup */
    uart_out_redirect(NULL);

    /* set default constants */
    initialize_default_values();

    encoder_setup();

    /* set SVPWM modulation frequency */
    set_mod_frequency(10000);

    /* setup kalman filter */
    kalman_filter_setup(&mc_session);

    /* setup interrupts */
    interrupt_init();

    /* allocate memory and initialize command hash map and variable hash map */
    command_list = stringHashMapCreate(17);
    variable_map = stringHashMapCreate(17);

    /* install command handlers and ModBus-RTU handlers */
    initialize_commands();
    modbus_handlers_init();

    /* register variables */
    initialize_variable_alias();

    /* setup DSP filters */
    initialize_filters();

    // 1/230400 * 14
    set_gp_timer_expire(60763);

    /* prepare GPIO levels */
    gpio_status_init();

    /* enable UART output */
    uart_out_redirect(&uart_tx_fifo);

#ifdef SELF_TEST
    /*int self_test = software_components_post();
    if(self_test == 0)
    {
    	printf("self test ok\n");
    }
    else
    {
    	printf("self test failed\n");
    }*/
#endif

    core_control_loop_disable();

    memset(scripts_storage, 0, SCRIPT_STORAGE_LEN);

    /* load linked variables from controller key-value map */
    initialize_kvmap();

    if(mc_session.encoder_configs.direction != 1 && mc_session.encoder_configs.direction != -1)
    {
    	mc_session.encoder_configs.direction = 1;
    }

    lprintf("rated: %fRPM\r\n", mc_session.encoder_configs.rated_rpm);
    mc_session.ref_config.enc_vel_max =
    		mc_session.encoder_configs.rated_rpm * 6.0f * mc_session.encoder_info.rev_pulse / 360.0f;
    lprintf("max %f enc cnts/s\r\n", mc_session.ref_config.enc_vel_max);

    /* detect encoder */
    encoder_detect();

    /* start core control loop */
    lprintf("core DSP timer on\r\n");
    dsp_core_timer_setup();

    lprintf("entering event handling loop\r\n");

	linear_init(&mc_session.fric_comp,
			mc_session.encoder_info.friction_comp_rpm_h, mc_session.encoder_info.friction_comp_torque_h,
			mc_session.encoder_info.friction_comp_rpm_l, mc_session.encoder_info.friction_comp_torque_l
	);


	/////////
	//script

	{
		script_list.script = NULL;
		script_load(&script_list, scripts_storage, SCRIPT_STORAGE_LEN);

		script_t *current = script_list.script;

		lprintf("load scripts: ");
		while(current)
		{
			lprintf(current->name);
			lprintf(",");

			current = current->next;
		}
		lprintf("\r\n");
	}

	//mc_session.state = CONTROL_PAUSED;

	////
	// delta
	delta_calculate_bounds(3200.0f);
	delta_kinematics_selftest();

    while(1)
    {
        uart_process_fifo();
        process_command();
        tick_second_process();

        vibration_fft_analyze(&mc_session);

        // -----------------
        status_led_process();
    }
}

