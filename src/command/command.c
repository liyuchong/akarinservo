/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "command.h"

command_handler_func *find_command(StringHashMap *command_list, char *cmd)
{
    /* find the command name in the hash map */
    StringHashMapNode *node = stringHashMapFind(command_list, cmd);

    /* command found */
    if(node)
    {
        return (command_handler_func *)node->value;
    }

    /* command not found */
    return NULL;
}

int execute_command(motor_control_session_t *session, StringHashMap *command_list, char *cmd_in)
{
#define MAX_ARG_LEN 256
#define MAX_CMD_LEN 128

    char cmd[MAX_ARG_LEN];
    strncpy(cmd, cmd_in, MAX_ARG_LEN);

    /* find first space */
    size_t len = strlen(cmd);
    size_t pos = 0;
    size_t param_start;
    for(size_t i = 0; i < len; i++)
    {
        if(cmd[i] == ' ')
        {
            pos = i;
            break;
        }
    }

    param_start = pos;

    /* no space */
    if(pos == 0) pos = len;

    char command[MAX_CMD_LEN];
    memset(command, 0, MAX_CMD_LEN);
    memcpy(command, cmd, pos);

    /* find the command name */
    command_handler_func *handler = find_command(command_list, command);

    if(handler)
    {
        /* trim argument list leading space */
        char *ptr = cmd + pos;
        while(*ptr == ' ') ptr++;

        /* now ptr is the start address of the argument list,
         * split the argument list. */
        int   argc = 0;
        char *argv[MAX_ARG_NUM];
        memset(argv, 0, MAX_ARG_NUM);

        char *last_cmd = ptr;
        char last_char = '\0';

        while(*ptr)
        {
            /* too many arguments */
            if(argc > MAX_ARG_NUM - 1)
            {
                return -3;
            }

            if(*ptr == '\"')
            {
                /* save current pointer */
                char *arg_head = ++ptr;

                /* search for end of quote */
                while(*ptr && *ptr++ != '\"');

                if(*(ptr - 1) != '\"')
                {
                    /* quotation not terminated */
                    return -2;
                }

                /* terminate the string */
                *(ptr - 1) = '\0';
                argv[argc++] = arg_head;
                last_cmd = ptr;
            }

            if(*ptr == ',' || (*ptr == ' ' && (last_char != ' ')))
            {
                last_char = *ptr;
                /* terminate the string */
                *ptr = '\0';

                if(ptr - last_cmd != 0)
                {
                    /* trim leading spaces before each argument */
                    char *arg_head = last_cmd;
                    while(*arg_head == ' ') arg_head++;

                    /* push this argument to argument list */
                    if(argc < MAX_ARG_NUM)
                    {
                        argv[argc++] = arg_head;
                    }
                }

                /* skip to the head of next argument */
                last_cmd = ptr + 1;
            }
            else
            {
                last_char = *ptr;
            }

            ptr++;
        }

        /* if we have more argument left */
        if(last_cmd != ptr)
        {
            if(argc < MAX_ARG_NUM)
            {
//            	/* trim leading spaces */
//                while(*last_cmd && *last_cmd == ' ') last_cmd++;
//
//                /* don't add to argument list if we reach the end */
//                if(*(last_cmd + 1) != '\0')
//                {
//                    argv[argc++] = last_cmd;
//                }
            	argv[argc++] = last_cmd;
            }
        }

        /* call the handler */
        return handler(session, argc, argv);
    }
    else
    {
        /* command not found */
        return -1;
    }
}

