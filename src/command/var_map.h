/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __VAR_MAP_H
#define __VAR_MAP_H

#include "../angular_pos.h"

/* variable data type */
typedef enum
{
    INT32,
    FLOAT32,
    INT64,
	ANGULAR_POS,
	ARRAY_FLT,
} variable_type;

/* variable access level */
typedef enum
{
	VAR_RO = 1,
	VAR_RW = 0,
} variable_permission;

#define GROUP_ALL	0

typedef struct
{
    void *ptr;
    variable_type type;
    int read_only;

    int array_len;

    int group;
} aliased_variable_t;

typedef struct
{
	char *name;
	uint32_t id;
} group_id_item_t;

static group_id_item_t var_group_ids[] = {
	{ .name = "ALL", 		.id = 0x0001 },
	{ .name = "CTRL_FRAM", 	.id = 0x0002 },
	{ .name = "ENC_FRAM", 	.id = 0x0004 },
	{ .name = "PERFORMANCE",.id = 0x0008 },
	{ .name = "SENSOR",		.id = 0x0010 },
	{ .name = "SERVO_VAR",  .id = 0x0020 },
	{ .name = "SCOPE",		.id = 0x0040 },
	{ .name = "MOTION",		.id = 0x0080 },
	{ .name = "FILTER",		.id = 0x0100 },
	{ .name = "VFD",		.id = 0x0200 },

	{ .name = "DELTA",		.id = 0x8000}
};

int group_id(char *name);

int group(char *name, ...);

static inline float extract_whatever(aliased_variable_t *var)
{
    if(var->type == INT32)
    {
        return (float)(*(int *)var->ptr);
    }
    if(var->type == INT64)
    {
        return (float)(*(int64_t *)var->ptr);
    }
    if(var->type == FLOAT32)
    {
        return *(float *)var->ptr;
    }
    if(var->type == ANGULAR_POS)
    {
    	return angular_pos_to_float(*(angular_pos_t *)var->ptr, PERIOD_REVOLUTION);
    }
    if(var->type == ARRAY_FLT)
    {
    	return 0.0f;
    }

    return 0.0f;
}

#endif /* __VAR_MAP_H */
