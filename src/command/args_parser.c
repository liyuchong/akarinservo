/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "args_parser.h"

static inline
void _args_token_addch(argc_parser_t *parser, int c)
{
    int n;

    n = (int)strlen(parser->argv_token);
    if (n == ARGV_TOKEN_MAX - 1)
    {
        return;
    }

    parser->argv_token[n] = c;
}

static inline
void _argv_token_finish(argc_parser_t *parser)
{
    if (parser->argc == ARGV_MAX)
    {
        return;
    }

    parser->argv[parser->argc++] = parser->argv_token;
    if ((parser->argv_token = malloc(ARGV_TOKEN_MAX * sizeof(char))) == NULL)
    {
    	lprintf("cannot allocate memory :_argv_token_finish\r\n");
    }
    else
    {
        memset(parser->argv_token, 0, ARGV_TOKEN_MAX * sizeof(char));
    }
}

int args_init(argc_parser_t *parser)
{
    parser->argc = 0;

    if ((parser->argv_token = malloc(ARGV_TOKEN_MAX * sizeof(char))) == NULL)
    {
        return 1;
    }

    memset(parser->argv_token, 0, ARGV_TOKEN_MAX * sizeof(char));

    return 0;
}

argc_parser_t *args_free(argc_parser_t *parser)
{
    for (int i = 0; i < parser->argc; i++)
    {
        free(parser->argv[i]);
    }

    return parser;
}

argc_parser_t *args_parse(argc_parser_t *parser, char *s)
{
    char container_start = 0;
    bool in_token = false, in_container = false, escaped = false;

    int len = (int)strlen(s) + 1;

    args_init(parser);
    for (int i = 0; i < len; i++)
    {
        char c = s[i];

        switch (c)
        {
            case ' ':
            case '\t':
            case '\n':
            case '\0':
                if (!in_token)
                {
                    continue;
                }

                if (in_container)
                {
                    _args_token_addch(parser, c);
                    continue;
                }

                if (escaped)
                {
                    escaped = false;
                    _args_token_addch(parser, c);
                    continue;
                }

                in_token = false;
                _argv_token_finish(parser);
                break;

            case '\'':
            case '\"':

                if (escaped)
                {
                    _args_token_addch(parser, c);
                    escaped = false;
                    continue;
                }

                if (!in_token)
                {
                    in_token = true;
                    in_container = true;
                    container_start = c;
                    continue;
                }

                if (in_container)
                {
                    if (c == container_start)
                    {
                        in_container = false;
                        in_token = false;
                        _argv_token_finish(parser);
                        continue;
                    }
                    else
                    {
                        _args_token_addch(parser, c);
                        continue;
                    }
                }

                lprintf("Parse Error! Bad quotes\n");
                break;

            case '\\':

                if (in_container && s[i + 1] != container_start)
                {
                    _args_token_addch(parser, c);
                    continue;
                }

                if (escaped)
                {
                    _args_token_addch(parser, c);
                    continue;
                }

                escaped = true;
                break;

            default:
                if (!in_token)
                {
                    in_token = true;
                }

                _args_token_addch(parser, c);
        }
    }

    if (in_container)
    {
        lprintf("args_parser: still in container\r\n");
        goto err_free_memory;
    }

    if (escaped)
    {
        lprintf("args_parser: unused escape\r\n");
        goto err_free_memory;
    }

    return parser;

    /* release memory before returning if errors occur */
err_free_memory:
    args_free(parser);
    return NULL;
}
