/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <inttypes.h>
#include "kalman/kalman_filter.h"
#include "command/string_hash_map.h"
#include "hw/sharc_hw.h"
#include "command/var_map.h"
#include "utils/algorithms.h"
#include "kinematics/motion_queue.h"
#include "kinematics/motion_planner.h"

#include "angular_pos.h"
#include <stdarg.h>

#include "db/kvmap.h"


    /* --------------------------
     *
     * Position control loop
     * set-point:    rot_pos_set
     * reference:    rot_pos_meas
     * feed-forward: d(rot_pos_set) / dt
     * -------------------------------------
     *               (vel_ff_observer)
     *                    +-----+
     *                .->-|  D  |->-----------------------------.
     *                |   +-----+   |                           |
     *                |             *-<--- [vel_feedforward]    |
     *                |             | (velocity feed-forward)   |
     *                |   +-----+   |   +-----+                 |
     * [rot_pos_set] --->-| PID |->-+->-| IIR |->-[rot_vel_set] |
     *                    +-----+       +-----+                 |
     *                       ^                                  |
     *                       |                                  |
     *                       `------<- [rot_pos_meas]           |
     *                                                          |
     * Velocity control loop                                    |
     * set-point:    rot_vel_set                                |
     * reference:    rot_vel_meas                               |
     * feed-forward: d^2(rot_pos_set) / dt^2                    |
     * -------------------------------------                    |
     *                                                          |
     *                .-----------------------------------------`
     *                | (torque_ff_observer)
     *                |   +-----+
     *                `->-|  D  |->-.
     *                    +-----+   |
     *                              *-<--- [torque_feedforward]
     *                              | (torque feed-forward)
     *                    +-----+   |
     * [rot_vel_set] --->-| PID |--->- [torque_set]
     *                    +-----+
     *                       ^
     *                       |
     *                       `------<- [rot_vel_meas]
     *
     * Torque control loop
     * set-point: torque_set
     * reference: torque_meas
     * -------------------------------------
     *
     *                    +-----+
     * [torque_set]  --->-| PID |--->- [current_cmd]
     *                    +-----+
     *                       ^
     *                       |
     *                       `------<- [torque_meas]
     *
     * Rotor magnetic flux control loop
     * set-point: 0
     * reference: flux_meas
     * -------------------------------------
     *
     *                    +-----+
     *             0 --->-| PID |--->- [flux_cmd]
     *                    +-----+
     *                       ^
     *                       |
     *                       `------<- [flux_meas]
     *
     * */

#ifndef MOTOR_CONTROL_H_
#define MOTOR_CONTROL_H_

#define AKARIN_VER_STRING "AKARIN Servo build " __DATE__ ", " __TIME__

#define CONTROL_FREQUENCY 		20000
#define CONTROL_VEL_DIV			4
#define CONTROL_VEL_FREQUENCY   (CONTROL_FREQUENCY /  CONTROL_VEL_DIV)

#define CONTROL_LOOP_TICK_TIME  (1.0f / CONTROL_FREQUENCY)
#define CONTROL_VEL_TICK_TIME	(1.0f / CONTROL_VEL_FREQUENCY)

#define VEL_LOOP_USE_PDFF		0

/*           2 pi
 * ----------------------
 * control loop frequency */
#define FREQ_INC_RATIO 			0.000314159

#ifndef M_2PI
#define M_2PI 6.283185307179586476925287
#endif

/* motor control state constants */
#define CONTROL_POSITION            0
#define CONTROL_VELOCITY            3
#define CONTROL_PAUSED              1
#define CONTROL_TORQUE              4
#define CONTROL_SET_ANGLE           5
#define CONTROL_MOTION              6
#define CONTROL_FB                  7
#define CONTROL_VFD					8

/* threshold for checking encoder misscode */
#define ENCODER_MISSCODE_CHECK_THRES 100

/* average resampler sampling count  */
/* displayed angular velocity */
#define ANG_VEL_RESAMPLE_AVG  2000
/* displayed torque */
#define TORQUE_RESAMPLE_AVG   2000
/* over current protection */
#define CURRENT_RESAMPLE_AVG  100

#define ANTI_COGGING			1
#define ANTI_COGGING_MAP_LEN	512

#define VAR_SCOPE_MAX_COLUMNS	3
#define VAR_SCOPE_MAX_LENGTH	2048
#define VAR_SCOPE_MAX_VAR_NAME	32

#define VIBS_FFT_ENABLED		1
#define VIBS_FFT_PTS			256

#define SESSION_IIR_FILTER_COUNT 10

#define CTRL_NAME_LEN				16
#define CTRL_ERROR_MSG_LEN			32

typedef struct
{
	char message[CTRL_ERROR_MSG_LEN];
	int id;
} motor_control_error;

#define ERROR_OVER_CURRENT	      0x01
#define ERROR_OUT_OF_CONTROL      0x02
#define ERROR_CTRL_EEPROM_CKSM    0x04
#define ERROR_ENCDR_EEPROM_CKSM   0x08
#define ERROR_ENCDR_MISSING    	  0x10
#define ERROR_TORQUE_LIM_PROCTECT 0x20

static motor_control_error _errors[] =
{
		{ "OVER CURRENT",          ERROR_OVER_CURRENT        },
		{ "OUT OF CONTROL",        ERROR_OUT_OF_CONTROL      },
		{ "CTRL EEPROM CKSM ERR",  ERROR_CTRL_EEPROM_CKSM    },
		{ "ENCDR EEPROM CKSM ERR", ERROR_ENCDR_EEPROM_CKSM   },
		{ "ENCDR MISSING",         ERROR_ENCDR_MISSING       },
		{ "TORQUE LIMIT PROTECT",  ERROR_TORQUE_LIM_PROCTECT },
};

#define SET_ERROR(err) (global_error |=  (err))
#define CLR_ERROR(err) (global_error &= ~(err))

#define CONTROL_STOP(__session) ((__session)->stop = 1)

/* digital I/O pins 0-2 */
#define DIO0	0x01
#define DIO1	0x02
#define DIO2	0x04

typedef struct
{
	/* direction register
	 *    0   |   1   |   2   | [3:31]
	 *  DIO0  |  DIO1 | DIO2  | reserved
	 *
	 *  1 = output
	 *  0 = input
	 *  */
	uint32_t dir;


	/* usage register
	 * [0:7] | [8:16] | [17:24] | [25:31]
	 * DIO0  | DIO1   | DIO2    | reserved
	 * */
	uint32_t usage;

	/* input:
	 * 0x10 limit switch CW   - normally open
	 * 0x11 limit switch CW   - normally closed
	 * 0x12 limit switch CCW  - normally open
	 * 0x13 limit switch CCW  - normally closed
	 *
	 * output:
	 * 0x20	motion complete
	 * 0x21 in position
	 * */
} dio_config_t;

typedef struct
{
	/* DIO input buffer register */
	uint32_t dio_in;

	/* DIO output buffer register */
	uint32_t dio_out;
} dio_state_t;

typedef struct
{
	uint32_t max, min, current;
	uint32_t pos_loop_tick, pos_loop_freq;
	uint32_t vel_loop_tick, vel_loop_freq;
} performance_cycle_counter_t;

typedef struct
{
	/* current sensor biases */
    int32_t hall_a_bias, hall_b_bias;
    /* current sensor coefficients */
    float hall_a_coeff, hall_b_coeff;

    /* address of the controller */
    int32_t local_address;

    int out_of_control_thres;

    dio_config_t dio_config;

    char name[CTRL_NAME_LEN];

    int pulse_input_mode;
    float pulse_step_amount;

    uint32_t crc32;

} motor_controller_config_t;

typedef struct
{
	/* average samplers for the scope */
	average_sampler_t scope_samplers[VAR_SCOPE_MAX_COLUMNS];

	/* buffers for the scope storage */
	float var_scope_buffer[VAR_SCOPE_MAX_COLUMNS][VAR_SCOPE_MAX_LENGTH];

	/* variable names to be recorded */
	char var_scope_var_name[VAR_SCOPE_MAX_COLUMNS][VAR_SCOPE_MAX_VAR_NAME];

	/* variable pointer to variables to be recorded */
	aliased_variable_t *var_scope_var[VAR_SCOPE_MAX_COLUMNS];

	/* variable sample length */
	int record_length;

	/* number of variables to be recorded for this session */
	int record_columns;

	int var_scope_div;
	int var_scope_trigger_count;
	int var_scope_sample_len;
	int var_scope_triggered;
} scope_session_t;

extern scope_session_t scope_session;

typedef struct
{
	float frequency;
	float electric_angle;
	float electric_angle_inc;
	float torque_ratio;
} motor_control_vfd_session;

typedef struct
{
	linear_interpolator pos_p;
	linear_interpolator vel_p, vel_i;
	linear_interpolator torque_ff;
} motor_auto_tuning_session;

#define FFT_STATE_WAITING		0
#define FFT_STATE_SAMPLING		1
#define FFT_STATE_COPYING		2

typedef struct
{
    float vibration_fft_output[VIBS_FFT_PTS * 2];
    float vibration_fft_sample_buffer[VIBS_FFT_PTS * 2];
    float vibration_fft_density_spectrum[VIBS_FFT_PTS / 2];

    int vibration_fft_state;
    int vibration_fft_update_count;
    int vibration_fft_current_sample;
    int vibration_fft_triggered;

    average_sampler_t fft_sampler;

    iir_filter_t notch_filter;
    int notch_filter_enabled;
} fft_session_t;

typedef struct
{
	int rev_pulse;
	int half_rev_pulse;
	float deg_per_pulse;
	float encoder_ratio;

	float friction_comp_rpm_h, friction_comp_rpm_l;
	float friction_comp_torque_h, friction_comp_torque_l;
} encoder_info_t;

typedef struct
{
	float ref_unit;
	float enc_count_per_ref_unit;
	float elec_gear_n, elec_gear_m;
	float enc_vel_max; /* counts per second */
} reference_config_t;

/* motor control session
 * This is an instance of a motor being controlled */
typedef struct
{
    /* control loop state */
    int state;
    int stop;

    motor_control_vfd_session vfd;

    reference_config_t ref_config;

    linear_interpolator fric_comp;

    motor_auto_tuning_session auto_tunning;

    encoder_info_t encoder_info;

    /* control loop tick count */
    uint64_t led_tick_count;
    uint64_t tick;

    /* encoder & motor related stuff */
	akarin_encoder_t encoder_configs;

	average_sampler_t velocity_sampler, position_sampler;

	int64_t encoder_count;
	int mechanical_angle_prev;

	/* electric angle of the motor, this is used to drive SVPWM module */
	float electric_angle;
	angular_pos_t rot_pos_meas_prev;

	/* enable encoder misscode check */
    int encoder_check_enabled;
    int encoder_raw;
    int encoder_misscode_count;

    /* current sensor related */
    median_filter_t current_filter_a;
    median_filter_t current_filter_b;
    average_sampler_t motor_current_sampler;

    float current_a, current_b;
    float hall_a_raw, hall_b_raw;
    float current_meas;
    float current_meas_filtered;

	motor_controller_config_t controller_configs;

	/* motion controller session */
	motion_controller_t motion;

	/* digital I/O and special switches */
    dio_state_t dio_state;
    int dio_home_switch;

    /* Kalman filter for angular velocity estimation,
     * angular acceleration estimation and angular jerk estimation */
    INSKalmanFilter angular_vel_kalman_filter;
    INSVector3f angular_state;

    /* differentiators for angular position and angular error */
    differentiator_angular_t pos_diff;
	differentiator_float_t pos_error_diff;
    differentiator_float_t vel_error_diff;
    differentiator_float_t acc_diff, jerk_diff;

    /* observers for finding desire velocity and torque */
    ab_observer_t vel_ff_observer;
    ab_observer_t torque_ff_observer;
    float pos_set_d, pos_set_dd;

    /* measured angular position and set point */
    angular_pos_t rot_pos_meas;
    angular_pos_t rot_pos_set;

    /* zero point offset */
    angular_pos_t x_offset;

    /* measured angular velocity and set point */
    float rot_vel_meas;
    float rot_vel_set;
    float rot_ang_vel_integral_max;
    iir_filter_t vel_filter;

    /* measured torque and set point */
    float torque_meas;
    float torque_set;

    /* measured rotor magnetic flux (control target -> 0) */
    float flux_meas;
    float flux_cmd;
    float flux_cmd_max;
    float flux_cmd_integral_max;

    /* output to SVPWM module */
    float current_cmd;

    /* position controller & tuner */
    pid_controller_t position_controller;
    pid_fuzzy_self_tuner pos_pid_tuner;

    /* velocity controller */
    pid_controller_t velocity_controller;
    pid_fuzzy_self_tuner vel_pid_tuner;

    pid_controller_t mag_flux_controller;
    pid_controller_t torque_controller;

    /* IIR notch filters for vibration suppression */
    iir_filter_t notch_filters[SESSION_IIR_FILTER_COUNT];
    int enabled_iir_notch_filters;

    /* angular error and change in angular error */
    float pos_error, pos_derror;
    float vel_error, vel_derror;

    /* low bandwidth velocity and torque data for performance monitoring */
    float ang_vel_display;
    float torque_display;

    average_sampler_t ang_vel_sampler;
    average_sampler_t torque_sampler;

    /* velocity control mode velocity setting */
    float rotor_angular_vel_control;

    /* free run mode torque setting */
    float free_run_torque;

    performance_cycle_counter_t performance;

    average_sampler_t vel_control_sampler;

    iir_filter_nth_t vel_command_filter;

    float set_angle;

    // TODO: move this to encoder settings
    float load_inertia_ratio;

    int32_t pulse_move_amount;

    fft_session_t fft_session;

} motor_control_session_t;

void motor_control_modulate(float vd, float vq, float sint, float cost);

void motor_control_execute(motor_control_session_t *session);

void motor_control_read_sensors(motor_control_session_t *session);

void motor_control_servo_loop(motor_control_session_t *session, float sint, float cost, float angle_rad);

void motion_control_execute(motor_control_session_t *session);

extern motor_control_session_t mc_session;
extern int echo_enable;
extern int debug_enable;

extern StringHashMap *variable_map;
extern StringHashMap *command_list;

extern KVMap *controller_kvmap;
extern KVMap *encoder_kvmap;

int initialize_commands(void);
int initialize_variable_alias(void);
int initialize_default_values(void);
int initialize_filters(void);
void kalman_filter_setup(motor_control_session_t *session);

void motor_control_initialize_components(void);

extern motion_queue_t motion_queue;

extern uint32_t global_error;

void motor_control_read_sensors(motor_control_session_t *session);

extern int uart_rx_idle;

static inline
void wait_for_tick(motor_control_session_t *session, int ticks)
{
	uint64_t current = session->tick;

	while(mc_session.tick - current < ticks)
	{
		idle();
	}
}


/* ModBus-RTU functions */

/**
 *  call the ModBus-RTU handler
 *
 *  @param data		a pointer to data received
 *  @param len		length of data
 */
extern void modbus_handle(char *data, int len);

/**
 *  calculate ModBus-RTU CRC16 checksum
 *
 *  @param buf		a pointer to data
 *  @param len		length of data
 *
 *  @return			calculated CRC16 value
 */
uint32_t modbus_crc16(uint8_t *buf, int len);

/**
 *  parse an IEEE754 floating point data(takes 4 bytes)
 *
 *  @param buf		a pointer to data
 *
 *  @return			parsed value
 */
float modbus_ieee754_parse(uint8_t *buf);

/**
 *  parse an INT32 integer data(takes 4 bytes)
 *
 *  @param buf		a pointer to data
 *
 *  @return			parsed value
 */
float modbus_int32_t_parse(uint8_t *buf);

/* motion controller commands */

/**
 *  go to a given target position
 *
 *  @param session 		motor control session
 *  @param target_pos 	target position
 *  @param target_acc	target acceleration
 *  @param target_vel	target velocity
 */
void motion_cmd_go(motor_control_session_t *session,
		float target_pos,
		float target_acc,
		float target_vel
);

/**
 *  trgger the motion queue
 *
 *  @param session 		motor control session
 */
void motion_cmd_trigger(motor_control_session_t *session);

/**
 *  zero the current position
 *
 *  @param session 		motor control session
 */
void motion_cmd_zero(motor_control_session_t *session);

/**
 *  find home
 *
 *  @param session 		motor control session
 */
void motion_cmd_home(motor_control_session_t *session);

/**
 *  pause the motion controller for a given duration
 *
 *  @param session 		motor control session
 *  @param duration		pause duration in ticks
 */
void motion_cmd_pause(motor_control_session_t *session, int duration);

void vibration_fft_analyze(motor_control_session_t *session);

void motor_control_fft(motor_control_session_t *session);

void process_command(void);

#endif /* MOTOR_CONTROL_H_ */
