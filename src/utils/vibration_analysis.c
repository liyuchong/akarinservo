/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "algorithms.h"
#include "../motor_control.h"

#if VIBS_FFT_ENABLED
void motor_control_fft(motor_control_session_t *session)
{
	/* do nothing if there's an FFT analysis request pending */
	if(session->fft_session.vibration_fft_triggered)
	{
		return;
	}

	switch(session->fft_session.vibration_fft_state)
	{
		case FFT_STATE_SAMPLING:
		{
			/* FFT: velocity error */
			int fft_sample_update_flag = 0;
			float vel_error_sample = average_sampler_update(
					&session->fft_session.fft_sampler, session->vel_derror, &fft_sample_update_flag
			);

			/* new sample available from average sampler */
			if(fft_sample_update_flag)
			{
				/* put sample into sample buffer */
				session->fft_session.
				vibration_fft_sample_buffer[session->fft_session.vibration_fft_current_sample++] = vel_error_sample;

				/* sample full */
				if(session->fft_session.vibration_fft_current_sample == VIBS_FFT_PTS)
				{
					session->fft_session.vibration_fft_current_sample = 0;

					/* start hardware FFT */
					fft256_dma_start(session->fft_session.vibration_fft_sample_buffer);

					/* FFT started, next state is waiting for FFT to complete */
					session->fft_session.vibration_fft_state = FFT_STATE_WAITING;
				}
			}

			break;
		}

		case FFT_STATE_WAITING:
		{
			/* FFT done, next state is copying */
			if(!fft_is_busy())
			{
				session->fft_session.vibration_fft_state = FFT_STATE_COPYING;
			}
			break;
		}

		case FFT_STATE_COPYING:
		{
			/* read FFT output from output buffer */
			// TODO: use DMA
			fft256_read(session->fft_session.vibration_fft_output);

			/* FFT copy done, issue an FFT analysis request */
			session->fft_session.vibration_fft_state = FFT_STATE_SAMPLING;
			session->fft_session.vibration_fft_triggered = 1;

			break;
		}
	}
}
#endif /* FFT_ENABLED */

void vibration_fft_analyze(motor_control_session_t *session)
{
	if(session->fft_session.vibration_fft_triggered)
	{
		for(int i = 0; i < VIBS_FFT_PTS >> 1; i++)
		{
			session->fft_session.vibration_fft_density_spectrum[i] =
					sqrt(session->fft_session.vibration_fft_output[i] * session->fft_session.vibration_fft_output[i] +
						 session->fft_session.vibration_fft_output[i + VIBS_FFT_PTS] *
					 	 session->fft_session.vibration_fft_output[i + VIBS_FFT_PTS]
			) / ((float)VIBS_FFT_PTS);
		}

		session->fft_session.vibration_fft_triggered = 0;
		session->fft_session.vibration_fft_update_count++;
	}
}
