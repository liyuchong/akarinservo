/*
 * log.c
 *
 *  Created on: Mar 18, 2017
 *      Author: Administrator
 */

#include "log.h"
#include "../hw/sharc_hw.h"

CB_DECLARE(uint32_t) log_buffer;

int log_init(void)
{
	CB_INIT(&log_buffer, uint32_t, LOG_LENGTH, malloc);
	return 0;
}

int log_append(char *str)
{
    int len = (int)strlen(str);

    /* combine 4 bytes to a word */
    for(int i = 0; i < len; i += 4)
    {
        uint32_t word = 0;

        for(int j = 4; j >= 0; j--)
        {
            if(i + j < len)
            {
                word <<= 8;
                word |= str[i + j];
            }
        }

        CB_APPEND(&log_buffer, word);
    }

    return 1;
}

void uart_putc(char c)
{
	if(c != 0)
	{
		uart_char_send(c);
	}
}

int print_log(void)
{
    uint32_t seq[LOG_LENGTH] = { 0 };
    CB_FILL_SEQUENCE(&log_buffer, seq, memcpy);

    for(int i = 0; i < LOG_LENGTH; i++)
    {
    	uart_putc((seq[i] & 0xff));
    	uart_putc((seq[i] >> 8) & 0xff);
    	uart_putc((seq[i] >> 16) & 0xff);
    	uart_putc((seq[i] >> 24) & 0xff);
    }

    return 0;
}
