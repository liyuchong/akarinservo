/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/platform.h>
#include "adi_initialize.h"

#include <cdef21479.h>
#include <def21479.h>
#include <sru.h>
#include <signal.h>
#include <sysreg.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>

#include <builtins.h>  /* Get definitions of compiler builtin functions */
#include <platform_include.h>      /* System and IOP register bit and address definitions. */
#include <processor_include.h>     /* Defines core timer support. */
#include <services/int/adi_int.h>  /* Interrupt Handler API header. */

#include "../utils/fifo.h"

extern CircularBuffer uart_tx_fifo;

extern CircularBuffer *ustdout;
extern CircularBuffer *uart_out;

#ifndef SHARC_HW_H__
#define SHARC_HW_H__

#define PLL_MULT            PLLM10
#define RDIV                (0x406)

#define CCLK 294912000
#define PCLK 147456000

typedef unsigned char uint8_t;

static int mc_pwm_period = 16000;//6000;
static int mc_pwm_period_half = 8000;//3000;

/* interrupt.c */
int core_control_loop_enabled;

void timer_isr(uint32_t iid, void *handlerArg);
void uart0_isr(uint32_t iid, void *handlerArg);

void dsp_core_timer_setup(void);
void core_control_loop_disable(void);
void core_control_loop_enable(void);

#define UART_RX_BUFFER_SIZE 1024
extern char uart_rx_buffer[UART_RX_BUFFER_SIZE];
extern int uart_rx_count;

void interrupt_init(void);
// --

void dsp_pll_setup(void);
void dsp_pwm_setup(void);
void update_pwm_comp(int a, int b, int c);

void dsp_uart_setup(void);

int uart_char_send(const char cVal);
int __uart_char_send_no_wait(const char c);

int uart_process_fifo(void);

void uart_out_redirect(CircularBuffer *fifo);

char *uart_str_send(char *str);

void wait(int time);

void dsp_spi_setup(void);

int Wait_For_SPIF(void);

int spi_byte_write(const int byByte);
int spi_byte_read(int *pbyByte);

int Wait_For_SPIF_b(void);
int spi_byte_write_b(const int byByte);
int spi_byte_read_b(int *pbyByte);

#define G1_OUT_HIGH() SRU(HIGH, DAI_PB13_I);
#define G1_OUT_LOW()  SRU(LOW, DAI_PB13_I);

#define LCD_SCLK_HIGH() SRU(HIGH, DAI_PB16_I);
#define LCD_SCLK_LOW()  SRU(LOW, DAI_PB16_I);

#define LCD_DO_HIGH() SRU(HIGH, DAI_PB12_I);
#define LCD_DO_LOW()  SRU(LOW, DAI_PB12_I);

#define LED_BLUE_HIGH()	SRU(HIGH, DAI_PB18_I);
#define LED_BLUE_LOW()	SRU(LOW, DAI_PB18_I);

#define LED_RED_HIGH()	SRU(HIGH, DAI_PB17_I);
#define LED_RED_LOW()	SRU(LOW, DAI_PB17_I);

#define LED_GREEN_HIGH()	SRU(HIGH, DAI_PB11_I);
#define LED_GREEN_LOW()	SRU(LOW, DAI_PB11_I);


#define EEPROM_CS_LOW()	SRU(LOW, DAI_PB10_I);
#define EEPROM_CS_HIGH()	SRU(HIGH, DAI_PB10_I);


////
#define ADC_CONVST_HIGH()  SRU(HIGH, DAI_PB07_I);
#define ADC_CONVST_LOW()   SRU(LOW, DAI_PB07_I);

#define ADC_CS_LOW()		SRU(LOW, DAI_PB14_I);
#define ADC_CS_HIGH()		SRU(HIGH, DAI_PB14_I);

#define EEPROM_CS_LOW()		SRU(LOW, DAI_PB10_I);
#define EEPROM_CS_HIGH()	SRU(HIGH, DAI_PB10_I);

#define CTRL_EEPROM_CS_LOW()	SRU(LOW, DAI_PB20_I);
#define CTRL_EEPROM_CS_HIGH()	SRU(HIGH, DAI_PB20_I);

void ad7367_select(void);

void ad7367_release(void);

void adc_read_data(float *channel1, float *channel2);

#define RS485_BUSY()     SRU(HIGH, DAI_PB02_I);
#define RS485_IDLE()     SRU(LOW, DAI_PB02_I);

void delay (int ms);

#define uputs(x) uart_str_send(x)

// DAIP8  -> ENCODER CS
// DAIP10 -> EEPROM  CS

// DAIP14 -> ADC_CS

// --------

void set_mod_frequency(float freq);

void controller_eeprom_release(void);

void controller_eeprom_select(void);

int controller_eeprom_busy(void);

void controller_eeprom_write_enable(void);

void controller_eeprom_write_byte(int address, uint8_t byte);

uint8_t controller_eeprom_read_byte(int address);

void controller_eeprom_driver_init(void);

void controller_eeprom_save(void *encoder, size_t len, int offset);

void *controller_eeprom_restore(void *encoder, size_t len, int offset);

void fram_write_byte(int address, uint8_t byte);

uint8_t fram_read_byte(int address);

int ctrl_fram_write_word(int address, uint32_t word);

uint32_t ctrl_fram_read_word(int address);

void dai_spi_setup(void);

void dsp_gpio_setup(void);

void gpio_status_init(void);

/* SPI driver */
typedef struct
{
	void 	(*spi_init)(void);

	int     (*spi_write_byte)(int);
	int     (*spi_read_byte)(int *);
	void    (*spi_select)(void);
	void 	(*spi_release)(void);
	void 	(*spi_dwell)(void);
} spi_device_t;

/* set timeout of GP timer in nanoseconds */
void set_gp_timer_expire(int ns);

/* enable GP timer */
static inline
void enable_gp_timer(void)
{
	*pTMSTAT = TIM0EN;
}

/* disable GP timer */
static inline
void disable_gp_timer(void)
{
	*pTMSTAT = TIM0DIS;
}

/* uprintf buffer */
#define PRINTF_LINE_BUF_LEN 512
static char _u_buffer[PRINTF_LINE_BUF_LEN];

/* UART version of printf */
static int uprintf(char *fmt, ...)
{
    int i, count;

    va_list args;
    va_start(args, fmt);
    count = vsnprintf(_u_buffer, PRINTF_LINE_BUF_LEN, fmt, args);
    va_end(args);

    for(i = 0; i < count; i++)
    {
        uart_char_send(_u_buffer[i]);
    }

    return count;
}

#endif /* SHARC_HW_H__ */
