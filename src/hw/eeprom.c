/*
 * eeprom.c
 *
 *  Created on: 2016��8��20��
 *      Author: yuchong
 */

#include "sharc_hw.h"

#define WREN 0x06
#define WRDI 0x04
#define RDSR 0x05
#define WRSR 0x01
#define READ 0x03
#define WRITE 0x02

/* MB85RS64V FRAM only */
#define RDID 0x9f

spi_device_t eeprom_device;

void controller_eeprom_release(void)
{
    SRU(HIGH, DAI_PB20_I);
    *pSPIBAUDB = 0;
}

void controller_eeprom_select(void)
{
    SRU(LOW, DAI_PB20_I);

    *pSPICTLB = (SPIEN|SPIMS|TIMOD1|WL8 | GM| (1 << 9) | (0 << 11) | (0 << 10));

    *pSPIBAUDB = 50;
}

void controller_eeprom_driver_init(void)
{
	eeprom_device.spi_select = controller_eeprom_select;
	eeprom_device.spi_write_byte = spi_byte_write_b;
	eeprom_device.spi_release = controller_eeprom_release;
	eeprom_device.spi_read_byte = spi_byte_read_b;
}

int controller_eeprom_busy(void)
{
	int byte;
	eeprom_device.spi_select();
    eeprom_device.spi_write_byte(RDSR);
    eeprom_device.spi_write_byte(0xff);
    eeprom_device.spi_read_byte(&byte);
    eeprom_device.spi_release();

    return byte & 0x01;
}

void controller_eeprom_write_enable(void)
{
	eeprom_device.spi_select();
	eeprom_device.spi_write_byte(WREN);
	eeprom_device.spi_release();
}

void fram_write_byte(int address, uint8_t byte)
{
	controller_eeprom_write_enable();

	eeprom_device.spi_select();
	eeprom_device.spi_write_byte(WRITE);
	eeprom_device.spi_write_byte(((address & 0xff00) >> 8));
	eeprom_device.spi_write_byte(address & 0x00ff);
    eeprom_device.spi_write_byte(byte);
    eeprom_device.spi_release();

    while(controller_eeprom_busy());
}

uint8_t fram_read_byte(int address)
{
	int byte;
	eeprom_device.spi_select();
	eeprom_device.spi_write_byte(READ);
	eeprom_device.spi_write_byte(((address & 0xff00) >> 8));
	eeprom_device.spi_write_byte(address & 0x00ff);
	eeprom_device.spi_write_byte(0xff);
	eeprom_device.spi_read_byte(&byte);
    eeprom_device.spi_release();

    return (uint8_t)(byte & 0xff);
}

int ctrl_fram_write_word(int address, uint32_t word)
{
	int addr = address << 2;
	fram_write_byte(addr,     (word & 0xff000000) >> 24);
	fram_write_byte(addr + 1, (word & 0x00ff0000) >> 16);
	fram_write_byte(addr + 2, (word & 0x0000ff00) >> 8);
	fram_write_byte(addr + 3, (word & 0x000000ff));

	return 0;
}

uint32_t ctrl_fram_read_word(int address)
{
	int addr = address << 2;

	uint32_t word = 0;

	word |= fram_read_byte(addr + 3);
	word |= fram_read_byte(addr + 2) << 8;
	word |= fram_read_byte(addr + 1) << 16;
	word |= fram_read_byte(addr)     << 24;

	return word;
}

void controller_eeprom_write_byte(int address, uint8_t byte)
{
	controller_eeprom_write_enable();

	eeprom_device.spi_select();
	eeprom_device.spi_write_byte(WRITE);
	eeprom_device.spi_write_byte(address);
    eeprom_device.spi_write_byte(byte);
    eeprom_device.spi_release();

    while(controller_eeprom_busy());
}

uint8_t controller_eeprom_read_byte(int address)
{
	int byte;
	eeprom_device.spi_select();
	eeprom_device.spi_write_byte(READ);
	eeprom_device.spi_write_byte(address);
	eeprom_device.spi_write_byte(0xff);
	eeprom_device.spi_read_byte(&byte);
    eeprom_device.spi_release();

    return (uint8_t)(byte & 0xff);
}

void controller_eeprom_save(void *encoder, size_t len, int offset)
{
    for(int i = 0; i < len; i++)
    {
    	uint32_t word = *((uint8_t *)(encoder) + i);

    	uint32_t bytes[4];

    	bytes[0] = (word & 0x000000ff);
    	bytes[1] = (word & 0x0000ff00) >> 8;
    	bytes[2] = (word & 0x00ff0000) >> 16;
    	bytes[3] = (word & 0xff000000) >> 24;

    	for(int j = 0; j < 4; j++)
    	{
    		controller_eeprom_write_byte(i * 4 + offset + j, bytes[j]);
    	}
    }
}

void *controller_eeprom_restore(void *encoder, size_t len, int offset)
{
    for(int i = 0; i < len; i++)
    {
    	/* read a word */
    	uint32_t word = 0;
    	for(int j = 0; j < 4; j++)
    	{
    		word <<= 8;
    		word |= controller_eeprom_read_byte(i * 4 + offset + (3 - j));
    	}

    	*((uint8_t *)(encoder) + i) = word;
    }

    return encoder;
}

