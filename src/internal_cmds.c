/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdarg.h>

#include "kalman/kalman_filter.h"
#include "hw/encoder.h"
#include "command/command.h"
#include "utils/log.h"

#include "var_commands.h"

#include "isp.h"
#include "script.h"

#include "kinematics/delta_kinematics.h"

COMMAND_HANDLER(zero)
{
    if(argc != 0) return 1;

    session->x_offset =
    		angular_pos_difference(angular_pos_from_float(0.0f, PERIOD_REVOLUTION),
    				session->rot_pos_set, PERIOD_REVOLUTION
			);

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(set_ctrl_state)
{
    if(argc != 1)
    {
        uprintf("usage: set_ctrl_state state");
        return 1;
    }

    session->state = atoi(argv[0]);
    uputs("accepted");

    return 0;
}

extern int motion_trigger;
extern float home_vel;
extern int home_found;

void motion_cmd_go(motor_control_session_t *session, float target_pos, float target_acc, float target_vel)
{
	// TODO: from rad/s^2 to angular_pos_t/s^2. fix this.
	target_acc *= 2607.594632098382;

	reference_config_t *cfg = &session->ref_config;

    float enc_count_per_mm = 1.0f / cfg->ref_unit * cfg->enc_count_per_ref_unit;

    float current_count = angular_pos_to_float(mc_session.rot_pos_meas, PERIOD_REVOLUTION);
    float start_mm = current_count / PERIOD_REVOLUTION * cfg->elec_gear_m / cfg->elec_gear_n;

    float end_mm = target_pos;
    float acc_max_mm = target_acc / enc_count_per_mm;
    float enc_vel_set = target_vel * 6.0f * mc_session.encoder_info.rev_pulse / 360.0f;
    float vel_max_mm = enc_vel_set / enc_count_per_mm;

    /* add motion to queue */
    motion_t motion_go = motion_make(1, end_mm, acc_max_mm, vel_max_mm);
    motion_go.motion_type = MOTION_GO;
    motion_enqueue(&motion_queue, motion_go);
}

void motion_cmd_delta_go(motor_control_session_t *session, char axis, INSVector3f start, INSVector3f end, float target_acc, float target_vel)
{
	/* calculate total distance to go */
	float dtg = sqrtf(pow(start.x - end.x, 2.0f) + pow(start.y - end.y, 2.0f) + pow(start.z - end.z, 2.0f));

	// TODO: from rad/s^2 to angular_pos_t/s^2. fix this.
	target_acc *= 2607.594632098382;

	reference_config_t *cfg = &session->ref_config;

	float enc_count_per_mm = 1.0f / 0.005 * 8.192;

	float current_count = 0.0f;
	float start_mm = current_count / PERIOD_REVOLUTION * 10 / 1;

	float end_mm = dtg;
	float acc_max_mm = target_acc / enc_count_per_mm;
	float enc_vel_set = target_vel * 6.0f * mc_session.encoder_info.rev_pulse / 360.0f;
	float vel_max_mm = enc_vel_set / enc_count_per_mm;

	lprintf("e: %f, acc: %f, vel: %f\n", dtg, acc_max_mm, vel_max_mm);

	/* add motion to queue */
	motion_t motion_go = motion_make(1, end_mm, acc_max_mm, vel_max_mm);
	motion_go.motion_type = MOTION_CUSTOM;

	switch(axis)
	{
		case AXIS_A: motion_go.custom_motion_axis = AXIS_A; break;
		case AXIS_B: motion_go.custom_motion_axis = AXIS_B; break;
		case AXIS_C: motion_go.custom_motion_axis = AXIS_C; break;

		default: motion_go.custom_motion_axis = -1;
	}

	motion_go.custom_motion_start = start;
	motion_go.custom_motion_end = end;

	motion_enqueue(&motion_queue, motion_go);

	delta_prev_pos_cmd = end;
}

void motion_cmd_trigger(motor_control_session_t *session)
{
	 motion_trigger = 1;
}

void motion_cmd_zero(motor_control_session_t *session)
{
	home_vel = 0.0f;
	home_found = 0;

    motion_t motion_home = motion_make(1, 0, 0, 0);
    motion_home.duration = 0;
    motion_home.motion_type = MOTION_ZERO;
    motion_enqueue(&motion_queue, motion_home);
}

void motion_cmd_home(motor_control_session_t *session)
{
	home_vel = 0.0f;
	home_found = 0;

    motion_t motion_home = motion_make(1, 0, 0, 0);
    motion_home.duration = 0;
    motion_home.motion_type = MOTION_HOME;
    motion_enqueue(&motion_queue, motion_home);
}

void motion_cmd_pause(motor_control_session_t *session, int duration)
{
    motion_t motion_pause = motion_make(1, 0, 0, 0);
    motion_pause.duration = duration;
    motion_pause.motion_type = MOTION_PAUSE;
    motion_enqueue(&motion_queue, motion_pause);
}

COMMAND_HANDLER(delta_start)
{
	if(argc != 4 && argc != 5 && argc != 6)
	{
		uputs("usage: delta_start <a|b|c> x y z acc vel");
		return 1;
	}

	if(strlen(argv[0]) != 1)
	{
		uputs("invalid axis");
		return 2;
	}

	float x = atof(argv[1]);
	float y = atof(argv[2]);
	float z = atof(argv[3]);

	float acc = session->encoder_configs.defulat_acc;
	float target_vel = session->encoder_configs.rated_rpm;

	if(argc > 4)
	{
		acc = atof(argv[4]);

		if(acc > session->encoder_configs.max_acc)
		{
			acc = session->encoder_configs.max_acc;
		}
	}

    if(argc > 5)
    {
        target_vel = atof(argv[5]);

        /* cannot faster than the rated RPM of the motor */
        if(target_vel > session->encoder_configs.rated_rpm)
        {
        	target_vel = session->encoder_configs.rated_rpm;
        }
    }

    /* calculate given position using delta IK */
    float pos;

    float t1, t2, t3;
    delta_inverse_kinematics(x, y, delta_top_z() + z, &t1, &t2, &t3);

    switch(argv[0][0])
    {
    	case AXIS_A: pos = t1; break;
    	case AXIS_B: pos = t2; break;
    	case AXIS_C: pos = t3; break;

    	default:
    		uputs("invalid axis");
    		return 3;
    }

    motion_cmd_go(&mc_session, pos, acc, target_vel);
    motion_cmd_trigger(session);

    delta_prev_pos_cmd.x = x;
    delta_prev_pos_cmd.y = y;
    delta_prev_pos_cmd.z = z;

    uputs("accepted");

	return 0;
}

COMMAND_HANDLER(d3b)
{
	if(argc != 5 && argc != 6 && argc != 7)
	{
		uputs("usage: d3b: <a|b|c> h x y z [acc] [vel]");
		return 1;
	}

	if(strlen(argv[0]) != 1)
	{
		uputs("invalid axis number length");
		return 2;
	}

	char axis = argv[0][0];

	if(axis != 'a' && axis != 'b' && axis != 'c')
	{
		uputs("invalid axis number");
		return 3;
	}

	float h = atof(argv[1]);

	INSVector3f end = {
			.x = atof(argv[2]),
			.y = atof(argv[3]),
			.z = atof(argv[4])
	};

    float target_acc =  session->encoder_configs.defulat_acc;
    float target_vel =  session->encoder_configs.rated_rpm;

    /* get target acceleration and target velocity */
    if(argc > 5)
    {
    	target_acc = atof(argv[5]);
    }

    if(argc > 6)
    {
    	target_vel = atof(argv[6]);
    }

	INSVector3f start = delta_prev_pos_cmd;

	INSVector3f p1 = {
			.x = delta_prev_pos_cmd.x,
			.y = delta_prev_pos_cmd.y,
			.z = h
	};

	INSVector3f p2 = {
			.x = end.x,
			.y = end.y,
			.z = h
	};

	/* calculate and enqueue */
		/* calculate total distance to go */
		float dtg = sqrtf(pow(start.x - end.x, 2.0f) + pow(start.y - end.y, 2.0f) + pow(start.z - end.z, 2.0f));

		// TODO: from rad/s^2 to angular_pos_t/s^2. fix this.
		target_acc *= 2607.594632098382;

		reference_config_t *cfg = &session->ref_config;

		float enc_count_per_mm = 1.0f / 0.005 * 8.192;

		float current_count = 0.0f;
		float start_mm = current_count / PERIOD_REVOLUTION * 10 / 1;

		float end_mm = dtg;
		float acc_max_mm = target_acc / enc_count_per_mm;
		float enc_vel_set = target_vel * 6.0f * mc_session.encoder_info.rev_pulse / 360.0f;
		float vel_max_mm = enc_vel_set / enc_count_per_mm;

		/* add motion to queue */
		motion_t motion_go = motion_make(1, end_mm, acc_max_mm, vel_max_mm);
		motion_go.motion_type = MOTION_B3D;

		switch(axis)
		{
			case AXIS_A: motion_go.custom_motion_axis = AXIS_A; break;
			case AXIS_B: motion_go.custom_motion_axis = AXIS_B; break;
			case AXIS_C: motion_go.custom_motion_axis = AXIS_C; break;

			default: motion_go.custom_motion_axis = -1;
		}

		/* copy control points */
		motion_go.custom_motion_start = start;
		motion_go.custom_b3d_p1 = p1;
		motion_go.custom_b3d_p2 = p2;
		motion_go.custom_motion_end = end;

		lprintf("start: %f %f %f\n", start.x, start.y, start.z);

		motion_enqueue(&motion_queue, motion_go);

		delta_prev_pos_cmd = end;

		uputs("accepted");

		return 0;
}

COMMAND_HANDLER(d3)
{
	if(argc != 13 && argc != 14 && argc != 15)
	{
		uputs("usage: d3 <a|b|c> x0 y0 z0 x1 y1 z1 x2 y2 z2 x3 y3 z3 [acc] [vel]");
		return 1;
	}

	if(strlen(argv[0]) != 1)
	{
		uputs("invalid axis number length");
		return 2;
	}

	char axis = argv[0][0];

	if(axis != 'a' && axis != 'b' && axis != 'c')
	{
		uputs("invalid axis number");
		return 3;
	}

	INSVector3f start = {
			.x = atof(argv[1]),
			.y = atof(argv[2]),
			.z = atof(argv[3])
	};

	INSVector3f p1 = {
			.x = atof(argv[4]),
			.y = atof(argv[5]),
			.z = atof(argv[6])
	};

	INSVector3f p2 = {
			.x = atof(argv[7]),
			.y = atof(argv[8]),
			.z = atof(argv[9])
	};

	INSVector3f end = {
			.x = atof(argv[10]),
			.y = atof(argv[11]),
			.z = atof(argv[12])
	};

	if(bound(start) || bound(p1) || bound(p2) || bound(end))
	{
		CONTROL_STOP(session);
		uputs("invalid coordinate");
		lprintf("bad coordinate\n");
		return 4;
	}

    float target_acc =  session->encoder_configs.defulat_acc;
    float target_vel =  session->encoder_configs.rated_rpm;

    /* get target acceleration and target velocity */
    if(argc > 13)
    {
    	target_acc = atof(argv[13]);
    }

    if(argc > 14)
    {
    	target_vel = atof(argv[14]);
    }

	/* calculate and enqueue */
	/* calculate total distance to go */
	float dtg = sqrtf(pow(start.x - end.x, 2.0f) + pow(start.y - end.y, 2.0f) + pow(start.z - end.z, 2.0f));

	// TODO: from rad/s^2 to angular_pos_t/s^2. fix this.
	target_acc *= 2607.594632098382;

	reference_config_t *cfg = &session->ref_config;

	float enc_count_per_mm = 1.0f / 0.005 * 8.192;

	float current_count = 0.0f;
	float start_mm = current_count / PERIOD_REVOLUTION * 10 / 1;

	float end_mm = dtg;
	float acc_max_mm = target_acc / enc_count_per_mm;
	float enc_vel_set = target_vel * 6.0f * mc_session.encoder_info.rev_pulse / 360.0f;
	float vel_max_mm = enc_vel_set / enc_count_per_mm;

	/* add motion to queue */
	motion_t motion_go = motion_make(1, end_mm, acc_max_mm, vel_max_mm);
	motion_go.motion_type = MOTION_B3D;

	switch(axis)
	{
		case AXIS_A: motion_go.custom_motion_axis = AXIS_A; break;
		case AXIS_B: motion_go.custom_motion_axis = AXIS_B; break;
		case AXIS_C: motion_go.custom_motion_axis = AXIS_C; break;

		default: motion_go.custom_motion_axis = -1;
	}

	/* copy control points */
	motion_go.custom_motion_start = start;
	motion_go.custom_b3d_p1 = p1;
	motion_go.custom_b3d_p2 = p2;
	motion_go.custom_motion_end = end;

	lprintf("start: %f %f %f\n", start.x, start.y, start.z);

	motion_enqueue(&motion_queue, motion_go);

	delta_prev_pos_cmd = end;

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(motion)
{
    if(argc == 0)
    {
        uputs("usage: motion g <pos> [acc] [tv]\r\n");
        uputs("usage: motion p <ticks>\r\n");
        uputs("usage: motion d <a|b|c> x y z\r [tacc] [tvel]\n");

        return 1;
    }

    if(strlen(argv[0]) != 1)
    {
        uputs("motion type length error");

        return 1;
    }

    char motion_type = argv[0][0];

    switch(argv[0][0])
    {
        case 'g':
        {
            /* get the position demand and offset */
        	float pos = atof(argv[1]);

            float target_acc =  session->encoder_configs.defulat_acc;
            float target_vel =  session->encoder_configs.rated_rpm;

            /* get acceleration demand */
            if(argc > 2)
            {
                target_acc = atof(argv[2]);
            }

            /* get velocity demand */
            if(argc > 3)
            {
                target_vel = atof(argv[3]);

                /* cannot faster than the rated RPM of the motor */
                if(target_vel > session->encoder_configs.rated_rpm)
                {
                	target_vel = session->encoder_configs.rated_rpm;
                }
            }

            motion_cmd_go(&mc_session, pos, target_acc, target_vel);

            break;
        }

        case 'o':
        {
        	if(argc != 2)
        	{
        		uputs("invalid arguments");
        		return 5;
        	}

        	if(argv[1][0] == 'h')
        	{
        	    motion_t motion_pause = motion_make(1, 0, 0, 0);
        	    motion_pause.motion_type = MOTION_G1_HIGH;
        	    motion_enqueue(&motion_queue, motion_pause);
        	}
        	else if(argv[1][0] == 'l')
        	{
        	    motion_t motion_pause = motion_make(1, 0, 0, 0);
        	    motion_pause.motion_type = MOTION_G1_LOW;
        	    motion_enqueue(&motion_queue, motion_pause);
        	}
        	else
        	{
        		uputs("unknown state");
        		return 6;
        	}

        	break;
        }

        case 'd':
        {
        	//motion d <a|b|c> x y z [tacc] [tvel]
        	if(argc != 5 && argc != 6 && argc != 7)
        	{
        		uputs("invalid number of arguments");
        		return 3;
        	}

        	INSVector3f end = {
					.x = atof(argv[2]),
					.y = atof(argv[3]),
					.z = atof(argv[4]),
        	};

        	if(strlen(argv[1]) != 1)
        	{
        		uputs("invalid axis parameter");
        		return 2;
        	}

        	char axis = argv[1][0];

            float target_acc =  session->encoder_configs.defulat_acc;
            float target_vel =  session->encoder_configs.rated_rpm;

            if(argc > 5)
            {
            	target_acc = atof(argv[5]);
            }

            if(argc > 6)
            {
            	target_vel = atof(argv[6]);
            }

            if(end.x > 220 || end.x < -220 || end.y > 220 || end.y < -220 || end.z > -100 || end.z < -250)
            {
            	mc_session.stop = 1;
            	lprintf("invalid coordinates: %f %f %f\n", end.x, end.y, end.z);
            }

        	motion_cmd_delta_go(session, axis, delta_prev_pos_cmd, end, target_acc, target_vel);
        	break;
        }

        case 'p':
        {
        	motion_cmd_pause(&mc_session, atoi(argv[1]));
            break;
        }

        case 'h':
        {
        	motion_cmd_home(&mc_session);
        	break;
        }

        case 'z':
        {
        	motion_cmd_zero(&mc_session);
        	break;
        }

        case 't':
        	motion_cmd_trigger(&mc_session);
        	break;
    	}

    	uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_pos)
{
    if(argc != 0) return 1;

    uprintf("pid:pos:%f,%f,%f",
            session->position_controller.p,
            session->position_controller.i,
            session->position_controller.d);

    return 0;
}

COMMAND_HANDLER(set_pid_pos)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_pos p, i, d");
        return 1;
    }

    session->position_controller.p = atof(argv[0]);
    session->position_controller.i = atof(argv[1]);
    session->position_controller.d = atof(argv[2]);

    session->position_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(set_pid_torque)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_torque p, i, d");
        return 1;
    }

    session->torque_controller.p = atof(argv[0]);
    session->torque_controller.i = atof(argv[1]);
    session->torque_controller.d = atof(argv[2]);

    session->torque_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_torque)
{
    if(argc != 0) return 1;

    uprintf("pid:torque:%f,%f,%f",
            session->torque_controller.p,
            session->torque_controller.i,
            session->torque_controller.d);

    return 0;
}

COMMAND_HANDLER(set_pid_flux)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_flux p, i, d");
        return 1;
    }

    session->mag_flux_controller.p = atof(argv[0]);
    session->mag_flux_controller.i = atof(argv[1]);
    session->mag_flux_controller.d = atof(argv[2]);

    session->mag_flux_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(get_pid_flux)
{
    if(argc != 0) return 1;

    uprintf("pid:flux:%f,%f,%f",
            session->mag_flux_controller.p,
            session->mag_flux_controller.i,
            session->mag_flux_controller.d);

    return 0;
}

COMMAND_HANDLER(get_pid_vel)
{
    if(argc != 0) return 1;

    uprintf("pid:vel:%f,%f,%f",
            session->velocity_controller.p,
            session->velocity_controller.i,
            session->velocity_controller.d);

    return 0;
}

COMMAND_HANDLER(get_name)
{
	if(argc != 0) return 1;

	uprintf("%s", mc_session.controller_configs.name);

	return 0;
}

COMMAND_HANDLER(set_name)
{
	if(argc != 1) return 1;

	strncpy(mc_session.controller_configs.name, argv[0], 64);

	uprintf("accepted");

	return 0;
}

COMMAND_HANDLER(set_pid_vel)
{
    if(argc != 3)
    {
        uprintf("usage: set_pid_vel p, i, d");
        return 1;
    }

    session->velocity_controller.p = atof(argv[0]);
    session->velocity_controller.i = atof(argv[1]);
    session->velocity_controller.d = atof(argv[2]);

    session->velocity_controller.integral = 0.0f;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(ver)
{
    if(argc != 0) return 1;
    uputs("AKARIN Servo build " __DATE__ ", " __TIME__);

    return 0;
}

COMMAND_HANDLER(ping)
{
    if(argc != 0) return 1;
    uputs("pong");

    return 0;
}

COMMAND_HANDLER(set_mod_freq)
{
    if(argc != 1)
    {
        uprintf("usage: set_mod_freq <freq>");

        return 1;
    }

    int mod_freq = atoi(argv[0]);

    if(mod_freq > 30000 || mod_freq < 5000)
    {
        uprintf("error: frequency out of range(%dHz).", mod_freq);

        return 2;
    }

    set_mod_frequency(mod_freq);

    uputs("accepted");
    return 0;
}

void print_variable_record(float *buffer, int len);

COMMAND_HANDLER(scope_read)
{
	if(argc != 2)
	{
		return 1;
	}

	int column = atoi(argv[1]);

	if(column >= VAR_SCOPE_MAX_COLUMNS)
	{
		return 2;
	}

	/* read scope data buffer */
	if(strcmp(argv[0], "val") == 0)
	{
		print_variable_record(scope_session.var_scope_buffer[column], scope_session.record_length);
	}

	/* read variable names */
	if(strcmp(argv[0], "name") == 0)
	{
		uputs(scope_session.var_scope_var_name[column]);
	}

	return 0;
}

COMMAND_HANDLER(scope)
{
    if(argc < 3)
    {
        uprintf("usage: scope <cmds> <length> <var>...");
        return 1;
    }

    /* extract pre-commands */
    char *ptr = argv[0];
    char *cmd_head = ptr;

    for(; *ptr; ptr++)
    {
        if(*ptr == '!')
        {
            *ptr = '\0';

            /* remove leading spaces */
            while(*cmd_head && *cmd_head == ' ') cmd_head++;

            char cmd_line[32] = { 0 };
            strncpy(cmd_line, cmd_head, 32);
            cmd_head = ptr + 1;


            uart_out_redirect(NULL);

            int ret = execute_command(session, command_list, cmd_line);

            if(ret != 0)
            {
                uprintf("\r\nerror while executing command: %s, return value = %d\r\n", cmd_line, ret);
            }

            uart_out_redirect(&uart_tx_fifo);
        }
    }

    /* read length or record and calculate columns of record */
    int record_length = atoi(argv[1]);
    int record_columns = argc - 2;

#ifdef SCOPE_VERBOSE
    for(int current_arg = 2; current_arg < argc; current_arg++)
    {
    	uputs("[var:");
    	uputs(argv[current_arg]);
    	uputs("]");
    }

    uprintf("[len:%d, col:%d]", record_length, record_columns);
#endif /* SCOPE_VERBOSE */

    if(record_length <= 0 || record_length > VAR_SCOPE_MAX_LENGTH)
    {
        uprintf("invalid value for <length>");
        return 1;
    }

    /* stop if the requested column is more than the available columns */
    if(record_columns > VAR_SCOPE_MAX_COLUMNS)
    {
    	uputs("too many columns");
    	return 2;
    }

    /* clear all existing variable pointers */
    for(int i = 0; i < VAR_SCOPE_MAX_COLUMNS; i++)
    {
    	scope_session.var_scope_var[i] = NULL;
    }

    /* search for variables */
    for(int current_arg = 2; current_arg < argc; current_arg++)
	{
    	aliased_variable_t *var = extract_variable(argv[current_arg]);

    	if(!var)
    	{
    		uprintf("variable %s does not exist", argv[current_arg]);
    		return 3;
    	}

    	/* store variable pointer */
    	scope_session.var_scope_var[current_arg - 2] = var;

    	/* copy variable name */
    	strncpy(scope_session.var_scope_var_name[current_arg - 2], argv[current_arg], VAR_SCOPE_MAX_VAR_NAME);
	}

    /* setup average samplers */
    for(int i = 0; i < VAR_SCOPE_MAX_COLUMNS; i++)
    {
    	average_sampler_init(&scope_session.scope_samplers[i], scope_session.var_scope_div);
    }

    /* trigger the scope */
    scope_session.var_scope_triggered = 0;
    scope_session.var_scope_trigger_count = record_length;
    scope_session.var_scope_sample_len = 0;
    scope_session.record_length = record_length;
    scope_session.record_columns = record_columns;

    uputs("accepted");

    return 0;
}

COMMAND_HANDLER(nfilter)
{
	if(argc != 2 && argc != 3)
	{
		uputs("usage: nfilter <tune|enable> <cf|enabled> [bw]");
		return 1;
	}

	if(strcmp("tune", argv[0]) == 0)
	{
		if(argc != 3)
		{
			uputs("usage: nfilter <tune|enable> <cf|enabled> [bw]");
			return 2;
		}

		/* convert frequency into fraction of sample rate */
		float center_freq = atof(argv[1]) / (float)CONTROL_FREQUENCY;
		float bandwidth = atof(argv[2]) / (float)CONTROL_FREQUENCY;

		/* get filter pointer */
		iir_filter_t *filter = &mc_session.fft_session.notch_filter;

		/* run IIR design algorithm */
		calculate_iir_coeff(
				bandwidth, center_freq,
				&filter->a0, &filter->a1, &filter->a2,
				&filter->b1, &filter->b2
		);

		uputs("filter tuned");
		return 0;

	}
	else if(strcmp("enable", argv[0]) == 0)
	{
		if(argc != 2)
		{
			uputs("usage: nfilter <tune|enable> <cf|enabled> [bw]");
			return 2;
		}

		int enabled = atoi(argv[1]);

		if(enabled != 0 && enabled != 1)
		{
			uputs("expect 1 or 0");
			return 3;
		}

		mc_session.fft_session.notch_filter_enabled = enabled;

		uputs("filter enable set");
		return 0;
	}
	else
	{
		return 1;
	}
}

COMMAND_HANDLER(notch_filter_tune)
{
	if(argc != 3)
	{
		uputs("usage: nfilter_tune <n> <cf> <bw>");
		return 1;
	}

	int filter_index = atoi(argv[0]);
	int enable = atoi(argv[3]);

	/* convert frequency into fraction of sample rate */
	float center_freq = atof(argv[1]) / (float)CONTROL_FREQUENCY;
	float bandwidth = atof(argv[2]) / (float)CONTROL_FREQUENCY;

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	/* get filter pointer */
	iir_filter_t *filter = &session->notch_filters[filter_index];

	/* run IIR design algorithm */
	calculate_iir_coeff(
			bandwidth, center_freq,
			&filter->a0, &filter->a1, &filter->a2,
			&filter->b1, &filter->b2
	);

	/* copy filter info */
	filter->cf = center_freq;
	filter->bw = bandwidth;

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(notch_filter_enable)
{
	if(argc != 2)
	{
		uputs("usage: nfilter <n> <enable>");
		return 1;
	}

	int filter_index = atoi(argv[0]);
	int enable = atoi(argv[1]);

	if(enable != 0 && enable != 1)
	{
		uputs("unexpected value for <enable>");
		return 1;
	}

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	if(enable)
	{
		session->enabled_iir_notch_filters |= (1 << filter_index);
	}
	else
	{
		session->enabled_iir_notch_filters &= ~(1 << filter_index);
	}

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(nfilter_info)
{
	if(argc != 1)
	{
		uputs("usage: nfilter_info <n>");
		return 1;
	}

	int filter_index = atoi(argv[0]);

	if(filter_index > SESSION_IIR_FILTER_COUNT - 1)
	{
		uputs("filter index out of bound");
		return 2;
	}

	/* print filter info
	 * filter n:cf,bw,enabled */
	iir_filter_t *filter = &session->notch_filters[filter_index];

	uprintf("filter %d:%g,%g,%d", filter_index, filter->cf, filter->bw,
			(session->enabled_iir_notch_filters & (1 << filter_index)) > 0);

	return 0;
}

COMMAND_HANDLER(nfilter_list)
{
	if(argc != 0)
	{
		uputs("usage: nfilter_list");
		return 1;
	}

	return 0;
}

COMMAND_HANDLER(get_error_what)
{
	if(argc != 1)
	{
		uputs("usage: get_error_what <id>");
		return 1;
	}

	int id = atoi(argv[0]);

	for(int i = 0; i < sizeof(_errors) / sizeof(motor_control_error); i++)
	{
		if(id == _errors[i].id)
		{
			uprintf("%d:%s", id, _errors[i].message);
			return 0;
		}
	}

	uprintf("%d:not found", id);

	return 2;
}

COMMAND_HANDLER(inertia_tune)
{
	if(argc != 1)
	{
		uputs("usage: inertia_tune <inertia ratio>");
		return 1;
	}

	float inertia_ratio = atof(argv[0]);

	if(inertia_ratio < 0.0001)
	{
		uputs("invalid inertia ratio");
		return 2;
	}

	/* use interpolators to calculate predicted loop gains */
	float pos_p, vel_p, vel_i, torque_ff;

	pos_p = linear_evaluate(&mc_session.auto_tunning.pos_p, inertia_ratio);
	vel_p = linear_evaluate(&mc_session.auto_tunning.vel_p, inertia_ratio);
	vel_i = linear_evaluate(&mc_session.auto_tunning.vel_i, inertia_ratio);
	torque_ff = linear_evaluate(&mc_session.auto_tunning.torque_ff, inertia_ratio);

	uprintf("pos_p: %f, vel_p: %f, vel_i: %f, torque_ff: %f",
			pos_p, vel_p, vel_i, torque_ff);

	mc_session.position_controller.p = pos_p;
	mc_session.position_controller.i = 0;
	mc_session.position_controller.d = 0;

	mc_session.velocity_controller.p = vel_p;
	mc_session.velocity_controller.i = vel_i;
	mc_session.velocity_controller.d = 0;

	mc_session.encoder_configs.torque_feedforward = torque_ff;

	return 0;
}

COMMAND_HANDLER(inertia_ratio_detect)
{
#define CHOOCH() \
	while(mc_session.tick - current_tick < detect_duration) idle();

	if(argc != 2)
	{
		uputs("usage: inertia_ratio_detect <torque> <duration>");
		return 1;
	}

	/* save current tick count and rotor position */
	uint64_t current_tick = mc_session.tick;
	angular_pos_t current_pos = mc_session.rot_pos_meas;

	float detect_torque = atof(argv[0]);
	int detect_duration = atoi(argv[1]);

	/* make sure arguments are within ranges */
	if(detect_torque > 0.5f || detect_torque < 0.0f)
	{
		uputs("invalid argument: detect_torque");
		return 2;
	}

	if(detect_duration > 5000 || detect_duration < 100)
	{
		uputs("invalid argument: detect_duration");
		return 3;
	}

	/* set control loop to torque mode */
	mc_session.free_run_torque = detect_torque;
	mc_session.state = CONTROL_TORQUE;

	/* wait for the control loop to chooch */
	CHOOCH();

	/* now set torque demand to zero */
    mc_session.free_run_torque = 0.0f;

    /* and record the rotor's position */
    float rotor_distance = angular_pos_difference_float(current_pos, mc_session.rot_pos_meas, PERIOD_REVOLUTION);

    /* now the motor is running, we need to stop it by applying an
     * opposite direction torque */

    /* record new tick count */
    current_tick = mc_session.tick;
    /* apply opposite torque */
	mc_session.free_run_torque = -detect_torque;

	/* chooch again */
	CHOOCH();

	/* and stop */
    mc_session.free_run_torque = 0.0f;

    /* calculate normalized inertia */
    float inertia_rotor_normalized =
    		mc_session.encoder_configs.inertia_rotor_calibrated /
			mc_session.encoder_configs.inertia_rotor_cal_torque /
			mc_session.encoder_configs.inertia_rotor_cal_duration;

    /* find average velocity */
    float rotor_vel = rotor_distance / detect_duration;

    /* calculate measured normalized inertia */
    float inertia_detected_normalized = rotor_vel / detect_torque / detect_duration;

    /* and we get inertia ratio */
    float inertia_ratio = fabsf(inertia_rotor_normalized / inertia_detected_normalized);

    uprintf("inertia ratio: %.0f%%, normalized velocity: %f",
    		inertia_ratio * 100.0f,
			rotor_vel);

	return 0;
}

COMMAND_HANDLER(fric_comp_update)
{
	if(argc != 0)
	{
		uputs("usage: fric_comp_update");
		return 1;
	}

	linear_init(&session->fric_comp,
			session->encoder_info.friction_comp_rpm_h, session->encoder_info.friction_comp_torque_h,
			session->encoder_info.friction_comp_rpm_l, session->encoder_info.friction_comp_torque_l
	);

	lprintf("fric_comp reevaluated");

	return 0;
}

COMMAND_HANDLER(fric_comp_test)
{
	uprintf("test: %f -> %f", atof(argv[0]), linear_evaluate(&session->fric_comp, atof(argv[0])));
	return 0;
}

COMMAND_HANDLER(queue)
{
	if(argc != 1)
	{
		uputs("usage: q f");
		return 1;
	}

	if(strlen(argv[0]) != 1)
	{
		uputs("invalid switch");
		return 2;
	}

	switch(argv[0][0])
	{
		case 'f': uprintf("%d", motion_queue_empty(&motion_queue)); break;

		default:
			uputs("unknown switch");
	}

	return 0;
}

COMMAND_HANDLER(resume)
{
	core_control_loop_disable();

    mc_session.state = CONTROL_VELOCITY;
    mc_session.rot_pos_set = mc_session.rot_pos_meas;
    mc_session.mechanical_angle_prev = motor_control_read_encoder(session->encoder_configs.direction);
    mc_session.rot_vel_meas = 0;
    mc_session.rot_vel_set = 0;

    for(int i = 0; i < 1000; i++)
    {
        ab_observer_update(&mc_session.vel_ff_observer, (float)angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION));
        ab_observer_update(&mc_session.torque_ff_observer, mc_session.vel_ff_observer.vk_1);

        mc_session.rot_vel_meas = iir_filter_update(&mc_session.vel_filter, mc_session.rot_vel_meas);
    }

	/* reset all the PID controllers */
    pid_controller_reset(&session->position_controller);
    pid_controller_reset(&session->velocity_controller);
    pid_controller_reset(&session->torque_controller);
    pid_controller_reset(&session->mag_flux_controller);

    /* clear motions */
    motion_queue_init(&motion_queue, MOTION_QUEUE_LENGTH);

    /* cancel current motion(if any) */
    motion_trigger = 0;

	mc_session.state = CONTROL_POSITION;
	mc_session.stop = 0;

    /* start the control loop */
	core_control_loop_enable();

	uprintf("position loop resumed. pos_set: %f",
			(float)angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION));

	return 0;
}

int kvmap_load(char *location)
{
	if(strcmp(location, "ctrl") == 0)
	{
		core_control_loop_disable();
		kvmap_load_variables(controller_kvmap);
		core_control_loop_enable();

		uputs("accepted");
	}
	else if(strcmp(location, "encoder") == 0)
	{
		core_control_loop_disable();
		kvmap_load_variables(encoder_kvmap);
		core_control_loop_enable();

		uputs("accepted");
	}
	else
	{
		uputs("unknown location");
		return 1;
	}

	return 0;
}

int kvmap_save(char *location)
{
	if(strcmp(location, "ctrl") == 0)
	{
		core_control_loop_disable();
		int n = kvmap_save_variables(controller_kvmap);
		core_control_loop_enable();

		uprintf("%d words written", n);
	}
	else if(strcmp(location, "encoder") == 0)
	{
		core_control_loop_disable();
		int n = kvmap_save_variables(encoder_kvmap);
		core_control_loop_enable();

		uprintf("%d words written", n);
	}
	else
	{
		uputs("unknown location");
		return 1;
	}

	return 0;
}

COMMAND_HANDLER(kvmap)
{
	if(argc != 2)
	{
		uputs("usage: kvmap <save|load> <location");
		return 1;
	}

	if(strcmp(argv[0], "save") == 0)
	{
		kvmap_save(argv[1]);
	}
	else if(strcmp(argv[0], "load") == 0)
	{
		kvmap_load(argv[1]);
	}

	return 0;
}

COMMAND_HANDLER(anti_cogging_calibrate)
{
	float cogging_torque_meas[ANTI_COGGING_MAP_LEN];

	if(fabsf(angular_pos_to_float(mc_session.rot_pos_set, PERIOD_REVOLUTION)) > 0.0001)
	{
		uputs("rotor position must be 0");
		return 1;
	}

	for(int i = 0; i < ANTI_COGGING_MAP_LEN; i++)
	{
		mc_session.encoder_configs.cogging_torque[i] = 0.0f;
	}

	int count = 0;
	for(int i = 0; i < 16384; i += 32)
	{
		mc_session.rot_pos_set = angular_pos_from_float(i, PERIOD_REVOLUTION);

		wait_for_tick(&mc_session, 1000);

		float torque_sample = 0.0f;
		for(int j = 0; j < 1000; j++)
		{
			torque_sample += mc_session.torque_meas;
			wait_for_tick(&mc_session, 2);
		}

		torque_sample /= 1000.0f;
		cogging_torque_meas[count++] = torque_sample;

	}

	for(int i = 0; i < ANTI_COGGING_MAP_LEN; i++)
	{
		mc_session.encoder_configs.cogging_torque[i] = cogging_torque_meas[i];
	}

	uputs("accepted");

	return 0;
}

COMMAND_HANDLER(pwr_cycle)
{
	uputs("accepted");

	*pSPICTL=0x0;
	*pSPIFLG=0x0;
	*pSPIDMAC=0x0;
	*pSPIBAUD=0x0;

	*pSYSCTL |= 0x01;

	return 0;
}

#include "utils/log.h"

COMMAND_HANDLER(log)
{
	if(argc != 1)
	{
		uputs("usage: log show");
		return 1;
	}

	print_log();

	return 0;
}

#include "utils/algorithms.h"
COMMAND_HANDLER(fft_test)
{
	for(int i = 0; i < 128; i++)
	{
		uprintf("%f,", mc_session.fft_session.vibration_fft_density_spectrum[i]);
	}

	return 0;
}

COMMAND_HANDLER(flash)
{
	adi_int_EnableInt(adi_int_EnableInt, false);
	core_control_loop_disable();
	mc_session.stop = 1;
	return flash_cmd(argc, argv);
}

int read_script_line(char *line)
{
	uputs(line);
	uputs("\r\n");

	return 0;
}

COMMAND_HANDLER(script)
{
	if(argc != 1 && argc != 2 && argc != 3)
	{
		uputs("usage: script <reset | save | load | new | read | delete | clear | append | list> [script] [content]");
		return 1;
	}

	char *cmd_exec = argv[0];
	char *script_name = argv[1];

	if(argc == 1 && strcmp(cmd_exec, "reset") == 0)
	{
		memset(scripts_storage, 0, SCRIPT_STORAGE_LEN);
		uputs("accepted");
		return 0;
	}

	if(argc == 1 && strcmp(cmd_exec, "save") == 0)
	{
		script_save(&script_list, scripts_storage);
		uputs("accepted");
		return 0;
	}

	if(argc == 1 && strcmp(cmd_exec, "load") == 0)
	{
		script_load(&script_list, scripts_storage, SCRIPT_STORAGE_LEN);
		uputs("accepted");
		return 0;
	}

	if(argc == 2 && strcmp(cmd_exec, "delete") == 0)
	{
		if(script_delete(&script_list, script_name))
		{
			uputs("accepted");
			return 0;
		}
		else
		{
			uputs("cannot delete script");
			return 1;
		}
	}

	// ----------

	if(argc == 2 && strcmp(cmd_exec, "new") == 0)
	{
		if(!script_add(&script_list, script_name, "#\n"))
		{
			uputs("cannot create script");
		}
		else
		{
			uputs("accepted");
		}

		return 0;
	}

	if(argc == 1 && strcmp(cmd_exec, "list") == 0)
	{
		script_t *current = script_list.script;

		while(current)
		{
			uputs(current->name);
			uputs(",");

			current = current->next;
		}

		return 0;
	}

	if(argc == 2 && strcmp(cmd_exec, "read") == 0)
	{
		script_t *s = script_find(&script_list, script_name, NULL);

		/* return if the specified script does not exist */
		if(!s)
		{
			uputs("no such script");
			return 1;
		}

		//script_read_lines(s, read_script_line);

		char *ptr = s->text;
		char line[1024] = { 0 };
		uint32_t count = 0;

		while(*ptr != '\0')
		{
			line[count++] = *ptr;

			if(*ptr == '\r')
			{
				line[count] = '\0';
				count = 0;
				uputs(line);
				uputs("\n");
			}

			ptr++;
		}

		return 0;
	}

	if(argc == 3 && strcmp(cmd_exec, "append") == 0)
	{
		script_t *s = script_find(&script_list, script_name, NULL);

		/* return if the specified script does not exist */
		if(!s)
		{
			uputs("no such script");
			return 1;
		}

		/* append content to the script */
		if(!(script_append(s, argv[2]) && script_append(s, "\r")))
		{
			/* no enough memory */
			uputs("cannot append script");
			return 2;
		}

		uputs("accepted");

		return 0;
	}

	if(argc == 2 && strcmp(cmd_exec, "clear") == 0)
	{
		script_t *s = script_find(&script_list, script_name, NULL);

		/* return if the specified script does not exist */
		if(!s)
		{
			uputs("no such script");
			return 1;
		}

		/* clear the script */
		if(!script_clear(s))
		{
			uputs("cannot clear script");
			return 2;
		}

		uputs("accepted");
		return 0;
	}

	return 2;
}

COMMAND_HANDLER(cmd_test)
{
	uprintf("%d args\r\n", argc);

	for(int i = 0; i < argc; i++)
	{
		uputs(argv[i]);
		uputs("\r\n");
	}

	return 0;
}

int initialize_commands(void)
{
	// TODO: asdasdsd
	script_list.script = NULL;

	stringHashMapInsert(command_list, "d3b", COMMAND(d3b));

	stringHashMapInsert(command_list, "d3", COMMAND(d3));

	stringHashMapInsert(command_list, "cmd_test", COMMAND(cmd_test));

	stringHashMapInsert(command_list, "q", COMMAND(queue));

	stringHashMapInsert(command_list, "fft_test", COMMAND(fft_test));
	stringHashMapInsert(command_list, "log", COMMAND(log));

	stringHashMapInsert(command_list, "script", COMMAND(script));

	stringHashMapInsert(command_list, "flash", COMMAND(flash));

	stringHashMapInsert(command_list, "nfilter", COMMAND(nfilter));

	stringHashMapInsert(command_list, "pwr_cycle", COMMAND(pwr_cycle));

	stringHashMapInsert(command_list, "agc", COMMAND(anti_cogging_calibrate));

    stringHashMapInsert(command_list, "zero", COMMAND(zero));
    stringHashMapInsert(command_list, "set_ctrl_state", COMMAND(set_ctrl_state));

    stringHashMapInsert(command_list, "set_pid_pos", COMMAND(set_pid_pos));
    stringHashMapInsert(command_list, "get_pid_pos", COMMAND(get_pid_pos));

    stringHashMapInsert(command_list, "set_pid_torque", COMMAND(set_pid_torque));
    stringHashMapInsert(command_list, "get_pid_torque", COMMAND(get_pid_torque));

    stringHashMapInsert(command_list, "set_pid_vel", COMMAND(set_pid_vel));
    stringHashMapInsert(command_list, "get_pid_vel", COMMAND(get_pid_vel));

    stringHashMapInsert(command_list, "set_pid_flux", COMMAND(set_pid_flux));
    stringHashMapInsert(command_list, "get_pid_flux", COMMAND(get_pid_flux));

    /* variable map commands */
    stringHashMapInsert(command_list, "var_get", COMMAND(var_get));
    stringHashMapInsert(command_list, "get",     COMMAND(var_get));
    stringHashMapInsert(command_list, "var_set", COMMAND(var_set));
    stringHashMapInsert(command_list, "set",     COMMAND(var_set));
    stringHashMapInsert(command_list, "var_get_prop", COMMAND(var_get_prop));
    stringHashMapInsert(command_list, "vars", COMMAND(vars));

    /* misc */
    stringHashMapInsert(command_list, "ver", COMMAND(ver));
    stringHashMapInsert(command_list, "ping", COMMAND(ping));

    stringHashMapInsert(command_list, "set_mod_freq", COMMAND(set_mod_freq));

    stringHashMapInsert(command_list, "scope", COMMAND(scope));

    stringHashMapInsert(command_list, "motion", COMMAND(motion));
    stringHashMapInsert(command_list, "m", COMMAND(motion));

    stringHashMapInsert(command_list, "nfilter_tune", COMMAND(notch_filter_tune));
    stringHashMapInsert(command_list, "nfilter_enable", COMMAND(notch_filter_enable));
    stringHashMapInsert(command_list, "nfilter_info", COMMAND(nfilter_info));
    stringHashMapInsert(command_list, "nfilter_list", COMMAND(nfilter_list));

    stringHashMapInsert(command_list, "get_error_what", COMMAND(get_error_what));

    stringHashMapInsert(command_list, "get_name", COMMAND(get_name));
    stringHashMapInsert(command_list, "set_name", COMMAND(set_name));

    stringHashMapInsert(command_list, "inertia_ratio_detect", COMMAND(inertia_ratio_detect));
    stringHashMapInsert(command_list, "inertia_tune", COMMAND(inertia_tune));

    stringHashMapInsert(command_list, "resume", COMMAND(resume));

    stringHashMapInsert(command_list, "kvmap", COMMAND(kvmap));

    stringHashMapInsert(command_list, "var_get_array", COMMAND(var_get_array));

    stringHashMapInsert(command_list, "scope_read", COMMAND(scope_read));

    stringHashMapInsert(command_list, "fric_comp_update", COMMAND(fric_comp_update));
    stringHashMapInsert(command_list, "fric_comp_test", COMMAND(fric_comp_test));

    stringHashMapInsert(command_list, "delta_start", COMMAND(delta_start));

    lprintf("commands registered\r\n");

    return 0;
}
