/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ANGULAR_POS_H_
#define ANGULAR_POS_H_

#include "hw/encoder.h"

#define PERIOD_REVOLUTION 	16384.0f

typedef struct
{
	int32_t rev;
	float deg;
} angular_pos_t;

static inline
float angular_pos_to_float(angular_pos_t ang, float period)
{
	return ang.rev * period + ang.deg;
}

static inline
angular_pos_t angular_pos_from_float(float pos, float period)
{
    angular_pos_t result;
    result.rev = pos / period;
    result.deg = pos - result.rev * period;

    return result;
}

static inline
float angular_pos_difference_float(angular_pos_t a, angular_pos_t b, float period)
{
	int rev_diff = a.rev - b.rev;
	return rev_diff * period + a.deg - b.deg;
}

static inline
angular_pos_t angular_pos_difference(angular_pos_t a, angular_pos_t b, float period)
{
	int rev_diff = a.rev - b.rev;
	return angular_pos_from_float(rev_diff * period + a.deg - b.deg, period);
}

static inline
angular_pos_t angular_pos_plus(angular_pos_t a, angular_pos_t b, float period)
{
	return angular_pos_from_float(
			angular_pos_to_float(a, period) + angular_pos_to_float(b, period),
			period);
}

static inline
angular_pos_t angular_pos_plus_pos(angular_pos_t ang, float pos, float period)
{
	ang.deg += pos;

	while(ang.deg > period)
	{
		ang.rev++;
		ang.deg -= period;
	}

	while(ang.deg < 0)
	{
		ang.rev--;
		ang.deg += period;
	}

	return ang;
}

typedef struct
{
    angular_pos_t z[5];
    float period;
} differentiator_angular_t;

static inline
differentiator_angular_t *differentiator_angular_init(differentiator_angular_t *d, float period)
{
    d->period = period;

    for(int i = 0; i < 5; i++)
    {
        d->z[i] = angular_pos_from_float(0.0f, period);
    }

    return d;
}

static inline
float differential_angular_pos(differentiator_angular_t *diff, angular_pos_t x)
{
    angular_pos_t ref = diff->z[0];

    for(int i = 0; i < 4; i++)
    {
        diff->z[i] = diff->z[i + 1];
    }

    diff->z[4] = x;

    const float a =
    angular_pos_to_float(angular_pos_difference(diff->z[0], ref, diff->period), diff->period);
    const float b =
    angular_pos_to_float(angular_pos_difference(diff->z[1], ref, diff->period), diff->period);
    const float c =
    angular_pos_to_float(angular_pos_difference(diff->z[3], ref, diff->period), diff->period);
    const float d =
    angular_pos_to_float(angular_pos_difference(diff->z[4], ref, diff->period), diff->period);

    return (1 / 12.0) * (-d + 8.0 * c - 8.0 * b + a);
}

#endif /* ANGULAR_POS_H_ */
