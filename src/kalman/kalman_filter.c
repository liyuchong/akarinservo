/*
 * Copyright (c) 2015, 2016 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "kalman_filter.h"

INSKalmanFilter *insKalmanFilterInit(INSKalmanFilter *filter,
    const INSMatrix33f A, const INSMatrix33f Q,
    const INSMatrix33f R, const INSMatrix33f B, const INSMatrix33f H
) {
    filter->A = A;
    filter->B = B;
    filter->Q = Q;
    filter->R = R;
    filter->B = B;
    filter->H = H;
    
    filter->previousX = insVector3fMake(0, 0, 0);
    filter->previousP = M33F_ZERO;
    
    return filter;
}

#pragma optimize_for_speed
INSVector3f insKalmanFilterUpdate(INSKalmanFilter *filter,
    const INSVector3f z, const INSVector3f u
) {
    /* PREDICTION
     * ----------
     */
    
    /* step 1 */
    /* x(k)bar = Ax(k-1) + Bu(k-1) */
    const INSVector3f x =
    insVector3fPlus(
        insM33fxV3f(filter->A, filter->previousX),
        insM33fxV3f(filter->B, u)
    );
    
    /* step 2 */
    /* P(k) = AP(k-1)A' + Q */
    const INSMatrix33f pkBar =
    insM33fPlus(
        insM33fxM33f(
            insM33fxM33f(filter->A,
                         filter->previousP),
            insM33fTranspose(filter->A)
        ),
        filter->Q
    );
    
    /* CORRECTION
     * ----------
     */
    
    /* step 3 */
    /* Kg = P(k)H'(HP(k)H' + R)^-1 */
    const INSMatrix33f s =
    insM33fInverse(
        insM33fPlus(
            insM33fxM33f(
                insM33fxM33f(filter->H, pkBar),
                insM33fTranspose(filter->H)
            ),
        filter->R)
    );
    
    const INSMatrix33f Kg =
    insM33fxM33f(insM33fxM33f(pkBar, insM33fInverse(filter->H)), s);
    
    /* step 4 */
    /* x(k) - output */
    /* x(k) = x(k)bar + Kg(z - H x(k)bar) */
    const INSVector3f xk =
    insVector3fPlus(x,
        insM33fxV3f(Kg,
            insVector3fMinus(z, insM33fxV3f(filter->H, x))
        )
    );
    
    /* step 5 */
    /* P(k) = (I - KgH)Pkbar */
    const INSMatrix33f P =
    insM33fxM33f(
        insM33fMinus(M33F_IDENTITY, insM33fxM33f(Kg, filter->H)), pkBar
    );
    
    /* store values */
    filter->previousX = xk;
    filter->previousP = P;
    
    return x;
}
