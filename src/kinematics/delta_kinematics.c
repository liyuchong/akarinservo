/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <math.h>

#include "delta_kinematics.h"
#include "../utils/log.h"

float _delta_radius, _delta_bottom_z, _delta_top_z;
INSVector3f delta_current_position;
INSVector3f delta_prev_pos_cmd;

INSVector3f bezier3d_3rd(bezier3d_3rd_interpolator_t *b3d, float t)
{
    float t1coeff = 1.0f - t;
    float t3coeff = pow(t1coeff, 3.0f);
    float t2coeff = pow(t1coeff, 2.0f);

    INSVector3f *pts = b3d->pts;

#define VECTOR_MUL(vector, m) (vector).x *= (m), (vector).y *= (m), (vector).z *= (m)

    INSVector3f p0t = pts[0];
    VECTOR_MUL(p0t, t3coeff);

    INSVector3f p1t = pts[1];
    VECTOR_MUL(p1t, t2coeff * t * 3.0f);

    INSVector3f p2t = pts[2];
    VECTOR_MUL(p2t, t1coeff * t * t * 3.0f);

    INSVector3f p3t = pts[3];
    VECTOR_MUL(p3t, t * t * t);

    INSVector3f result = insVector3fPlus(insVector3fPlus(insVector3fPlus(p0t, p1t), p2t), p3t);

    return result;
}

INSVector3f interpolate_3d_linear(INSVector3f start, INSVector3f end, float t)
{
    float t1 = 1.0f - t;

    INSVector3f result = {
        .x = (start.x * t1 + end.x * t),
        .y = (start.y * t1 + end.y * t),
        .z = (start.z * t1 + end.z * t)
    };

    return result;
}

/*
 * original code by
 * http://forums.trossenrobotics.com/tutorials/introduction-129/delta-robot-kinematics-3276/
 * */

const float e = 69.85f;     // Distance from center of the end effector to the center of the end effector joints
const float f = 76.2f;     // Distance from center of machine to center of each motor shaft
const float re = 590.55f;     // From the EE joint to the elbow joint
const float rf = 273.05f;     // Distance from motor shaft to elbow

// trigonometric constants
const float sqrt3   = 1.7320508075688772f;
const float pi      = 3.141592653;
const float sin120  = 0.866025388;
const float cos120  = -0.5;
const float tan60   = 1.73205078;
const float sin30   = 0.5;
const float tan30   = 0.577350259;

// forward kinematics: (theta1, theta2, theta3) -> (x0, y0, z0)
// returned status: 0=OK, -1=non-existing position
int delta_fwd_kinematics(float theta1, float theta2, float theta3, float *x0, float *y0, float *z0)
{
    float t = (f - e) * tan30 / 2;
    float dtr = pi / (float)180.0f;

    theta1 *= dtr;
    theta2 *= dtr;
    theta3 *= dtr;

    float y1 = -(t + rf * cos(theta1));
    float z1 = -rf * sin(theta1);

    float y2 = (t + rf * cos(theta2)) * sin30;
    float x2 = y2 * tan60;
    float z2 = -rf * sin(theta2);

    float y3 = (t + rf * cos(theta3)) * sin30;
    float x3 = -y3 * tan60;
    float z3 = -rf * sin(theta3);

    float dnm = (y2-y1) * x3 - (y3 - y1) * x2;

    float w1 = y1 * y1 + z1 * z1;
    float w2 = x2 * x2 + y2 * y2 + z2 * z2;
    float w3 = x3 * x3 + y3 * y3 + z3 * z3;

    // x = (a1*z + b1)/dnm
    float a1 = (z2 - z1) * (y3 - y1) - (z3 - z1) * (y2 - y1);
    float b1 = -((w2 - w1) * (y3 - y1) - (w3 - w1) * (y2 - y1)) / 2.0f;

    // y = (a2*z + b2)/dnm;
    float a2 = -(z2 - z1) * x3 + (z3 - z1) * x2;
    float b2 = ((w2 - w1) * x3 - (w3 - w1) * x2) / 2.0;

    // a*z^2 + b*z + c = 0
    float a = a1 * a1 + a2*a2 + dnm * dnm;
    float b = 2.0f * (a1 * b1 + a2 * (b2 - y1 * dnm) - z1 * dnm * dnm);
    float c = (b2 - y1 * dnm)*(b2 - y1 * dnm) + b1 * b1 + dnm * dnm * (z1 * z1 - re * re);

    // discriminant
    float d = b * b - (float)4.0 * a * c;

    if (d < 0)
    {
    	return -1; // non-existing point
    }

    *z0 = -(float)0.5 * (b + sqrt(d)) / a;
    *x0 = (a1 * *z0 + b1) / dnm;
    *y0 = (a2 * *z0 + b2) / dnm;

    return 0;
}

// inverse kinematics
// helper functions, calculates angle theta1 (for YZ-pane)
static inline
int delta_calcAngleYZ(float x0, float y0, float z0, float *theta)
{
    float y1 = -0.5 * 0.57735 * f; // f/2 * tg 30
    y0 -= 0.5 * 0.57735 * e;    // shift center to edge
    // z = a + b*y
    float a = (x0 * x0 + y0 * y0 + z0 * z0 + rf * rf - re * re - y1 * y1) / (z0 * 2.0f);
    float b = (y1 - y0) / z0;

    // discriminant
    float d = -(a + b * y1) * (a + b * y1) + rf * (b * b * rf + rf);
    if (d < 0)
    {
        return -1; // non-existing point
    }

    float yj = (y1 - a * b - sqrt(d)) / (b * b + 1); // choosing outer point
    float zj = a + b * yj;
    *theta = 180.0 * atan(-zj / (y1 - yj)) / pi + ((yj > y1) ? 180.0 : 0.0);
    return 0;
}

int delta_inverse_kinematics(float x0, float y0, float z0, float *theta1, float *theta2, float *theta3)
{
    *theta1 = *theta2 = *theta3 = 0.0f;

    int status = delta_calcAngleYZ(x0, y0, z0, theta1);

    if (status == 0)
    {
        status = delta_calcAngleYZ(x0 * cos120 + y0 * sin120, y0 * cos120 - x0 * sin120, z0, theta2);
    }
    if (status == 0)
    {
        status = delta_calcAngleYZ(x0 * cos120 - y0 * sin120, y0 * cos120 + x0 * sin120, z0, theta3);
    }

    if(status == 0)
    {
    	*theta1 = - *theta1;
    	*theta2 = - *theta2;
    	*theta3 = - *theta3;
    }

    return status;
}

void delta_calculate_bounds(float s)
{
    float maxx = -e - f - re - rf;
    float maxz = maxx;
    float minz = -maxx;
    float sd = 360.0f / s;
    float z;

    for(z = 0; z < s; z++)
    {
        float x, y, z1;
        int status = delta_fwd_kinematics(z * sd, z * sd, z * sd, &x, &y, &z1);

        if(status == 0)
        {
            if(minz > z1) minz = z1;
            if(maxz < z1) maxz = z1;
        }
    }

    float middlez = (maxz + minz) * 0.5;
    float original_dist = (maxz - middlez);
    float dist = original_dist * 0.5;
    float sum = 0 ;
    float mint1 =  360;
    float maxt1 = -360;
    float mint2 =  360;
    float maxt2 = -360;
    float mint3 =  360;
    float maxt3 = -360;

    float r[8][3];
    int status[8];

    do
    {
        sum += dist;
        status[0] = delta_inverse_kinematics(+sum, +sum, middlez + sum, &r[0][0], &r[0][1], &r[0][2]);
        status[1] = delta_inverse_kinematics(+sum, -sum, middlez + sum, &r[1][0], &r[1][1], &r[1][2]);
        status[2] = delta_inverse_kinematics(-sum, -sum, middlez + sum, &r[2][0], &r[2][1], &r[2][2]);
        status[3] = delta_inverse_kinematics(-sum, +sum, middlez + sum, &r[3][0], &r[3][1], &r[3][2]);
        status[4] = delta_inverse_kinematics(+sum, +sum, middlez - sum, &r[4][0], &r[4][1], &r[4][2]);
        status[5] = delta_inverse_kinematics(+sum, -sum, middlez - sum, &r[5][0], &r[5][1], &r[5][2]);
        status[6] = delta_inverse_kinematics(-sum, -sum, middlez - sum, &r[6][0], &r[6][1], &r[6][2]);
        status[7] = delta_inverse_kinematics(-sum, +sum, middlez - sum, &r[7][0], &r[7][1], &r[7][2]);

        if(status[0] !=0 || status[1] !=0 || status[2] !=0 || status[3] !=0 ||
           status[4] !=0 || status[5] !=0 || status[6] !=0 || status[7] !=0 )
        {
            sum -= dist;
            dist *= 0.5;
        }
        else
        {
            for(int i = 0; i < 8; i++)
            {
                if(mint1 > r[i][0]) mint1 = r[i][0];
                if(maxt1 < r[i][0]) maxt1 = r[i][0];
                if(mint2 > r[i][1]) mint2 = r[i][1];
                if(maxt2 < r[i][1]) maxt2 = r[i][1];
                if(mint3 > r[i][2]) mint3 = r[i][2];
                if(maxt3 < r[i][2]) maxt3 = r[i][2];
            }
        }
    } while(original_dist > sum && dist > 0.1);

    lprintf("Delta IK:\n");
    lprintf("middle z: %f\n", middlez);
    lprintf("y: %f -> %f\n", -sum, sum);
    lprintf("z: %f -> %f\n", middlez - sum, middlez + sum);

    lprintf("t1: %f -> %f\n", mint1, maxt1);
    lprintf("t2: %f -> %f\n", mint2, maxt2);
    lprintf("t3: %f -> %f\n", mint3, maxt3);

    _delta_bottom_z = middlez - sum;
    _delta_top_z = middlez + sum;
    _delta_radius = sum;

    lprintf("bottom z: %f\n", delta_bottom_z());
}

int delta_kinematics_selftest(void)
{
	delta_kinematics_test_t delta_self_tests[] =
	{
		{ .x = 0.0f, .y = 0.0f, .z = delta_bottom_z() },
		{ .x =  delta_radius(), .y =  delta_radius(), .z = delta_bottom_z() + delta_radius() },
		{ .x = -delta_radius(), .y =  delta_radius(), .z = delta_bottom_z() + delta_radius() },
		{ .x = -delta_radius(), .y = -delta_radius(), .z = delta_bottom_z() + delta_radius() },
		{ .x =  delta_radius(), .y = -delta_radius(), .z = delta_bottom_z() + delta_radius() }
	};

	int test_case_cnt = sizeof(delta_self_tests) / sizeof(delta_kinematics_test_t);

	/* run each test cases */
	for(int i = 0; i < test_case_cnt; i++)
	{
		float t1, t2, t3;
		float x = delta_self_tests[i].x, y = delta_self_tests[i].y, z = delta_self_tests[i].z;

		/* calculate inverse kinematics */
		int status = delta_inverse_kinematics(x, y, z, &t1, &t2, &t3);

		if(status != 0)
		{
			goto __selftest_failed;
		}

		/* calculate forward kinematics */
		float x_out, y_out, z_out;
		delta_fwd_kinematics(t1, t2, t3, &x_out, &y_out, &z_out);

		if(fabs(x - x_out) > DELTA_FLT_EPS || fabs(y - y_out) > DELTA_FLT_EPS || fabs(z - z_out) > DELTA_FLT_EPS)
		{
			goto __selftest_failed;
		}
	}

	lprintf("delta IK self test: OK(%d cases)\n", test_case_cnt);
	return 0;

__selftest_failed:
	lprintf("delta OK self test: Failed\n");
	return 1;
}
