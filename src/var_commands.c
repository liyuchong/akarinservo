/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "var_commands.h"

static void report_variable(char *name, char op, int gorup_num)
{
	if(strcmp(name, "dummy") == 0)
	{
		return;
	}

	aliased_variable_t *var_check = (aliased_variable_t *)extract_variable(name);

	if(!(var_check->group & gorup_num))
	{
		return;
	}

	switch(op)
	{
		/* variable name list */
		case 'l':
		{
			uputs(name);
            uputs(",");
            break;
		}

		/* variable value list */
		case 'v':
		{
			aliased_variable_t *var = (aliased_variable_t *)extract_variable(name);

			if(var)
			{
				switch(var->type)
				{
					case INT32:
						uprintf("%d,", VARIABLE(name, int));
						break;
					case FLOAT32:
						uprintf("%g,", VARIABLE(name, float));
						break;
					case INT64:
						uprintf("%lld,", VARIABLE(name, int64_t));
						break;
					case ANGULAR_POS:
						uprintf("%g,", angular_pos_to_float(VARIABLE(name, angular_pos_t), PERIOD_REVOLUTION));
						break;
					case ARRAY_FLT:
						uprintf("array_float[%d],", VAR_ARRAY_LEN(name));
						break;

					default:
						uprintf("variable type not recognized,");
				}
			}
			else
			{
				uputs("no such variable,");
			}

			break;
		}

		/* variable type list */
		case 't':
		{
			aliased_variable_t *var = (aliased_variable_t *)extract_variable(name);

			if(var)
			{
				switch(var->type)
				{
					case INT32: 		uputs("i32,");	break;
					case FLOAT32: 		uputs("f32,"); 	break;
					case INT64: 		uputs("i64,"); 	break;
					case ANGULAR_POS: 	uputs("ang,"); 	break;
					case ARRAY_FLT:		uputs("arrf,"); break;

					default: uprintf("variable type not recognized,");
				}
			}
			else
			{
				uputs("no such variable,");
			}

			break;
		}

		/* variable read-only list */
		case 'r':
		{
			aliased_variable_t *var = (aliased_variable_t *)extract_variable(name);

			if(var)
			{
				uprintf("%d,", var->read_only);
			}
			else
			{
				uputs("no such variable,");
			}

			break;
		}

		default:
		{
			uputs("???,");
		}
	}
}

COMMAND_HANDLER(vars)
{
	if(argc != 1 && argc != 2)
	{
		uputs("usage: vars <l | v | t | r | g> [group]");

		return 1;
	}

	if(strlen(argv[0]) != 1)
	{
		uputs("Illegal operation code");

		return 2;
	}


	if(argv[0][0] == 'g')
	{
		for(int i = 0; i < sizeof(var_group_ids) / sizeof(group_id_item_t); i++)
		{
			uputs(var_group_ids[i].name);
			uputs(",");
		}

		return 0;
	}

	char op = argv[0][0];
	char *group = "ALL";

	/* get selected group */
	if(argc == 2)
	{
		group = argv[1];
	}

	int group_num = group_id(group);

	if(group_num == -1)
	{
		uputs("group does not exist");
		return 3;
	}

    for(int i = 0; i < variable_map->hashArraySize; i++)
    {
        StringHashMapNode *current = &variable_map->root[i];

        if(current->next)
        {
        	report_variable(current->key, op, group_num);
        }

        while (current)
        {
            current = current->next;
            if(current)
            {
                if(current->next)
                {
                	report_variable(current->key, op, group_num);
                }
            }
        }
    }

    return 0;
}

COMMAND_HANDLER(var_set)
{
    /* var_set var value [type] */

    if(argc != 2 && argc !=3)
    {
        uprintf("usage: var_set var, value");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("error: var %s not found", argv[0]);
        return 2;
    }

    if(var->read_only)
    {
        uprintf("error: var %s is read only", argv[0]);
        return 3;
    }

    switch(var->type)
    {
        case INT32: 	VARIABLE(argv[0], int) 		= atoi(argv[1]); 				break;
        case FLOAT32: 	VARIABLE(argv[0], float) 	= atof(argv[1]); 				break;
        case INT64: 	VARIABLE(argv[0], int64_t) 	= strtoll(argv[1], NULL, 10); 	break;

        case ANGULAR_POS:
        	VARIABLE(argv[0], angular_pos_t) = angular_pos_from_float(atof(argv[1]), PERIOD_REVOLUTION);
        	break;

        case ARRAY_FLT:
        	uprintf("array write not implemented");
        	break;

        default:
            uprintf("variable type not recognized");
    }

    uputs("accepted");
    return 0;
}

COMMAND_HANDLER(var_get)
{
    if(argc != 1)
    {
        uprintf("usage: var_get var");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("var %s not found", argv[0]);
        return 2;
    }

    switch(var->type)
    {
        case INT32:
            uprintf("%s:%d", argv[0], VARIABLE(argv[0], int));
            break;
        case FLOAT32:
            uprintf("%s:%g", argv[0], VARIABLE(argv[0], float));
            break;
        case INT64:
            uprintf("%s:%lld", argv[0], VARIABLE(argv[0], int64_t));
            break;
        case ANGULAR_POS:
        	uprintf("%s:%g", argv[0], angular_pos_to_float(VARIABLE(argv[0], angular_pos_t), PERIOD_REVOLUTION));
        	break;
        case ARRAY_FLT:
        	uprintf("%s:array[%d]", argv[0], VAR_ARRAY_LEN(argv[0]));
        	break;

        default:
            uprintf("variable type not recognized");
    }

    return 0;
}

COMMAND_HANDLER(var_get_array)
{
#define ASCII   0
#define IEEE754 1

	/* default format = ASCII */
	int format = ASCII;

    if(argc != 1 && argc != 2)
    {
        uprintf("usage: var_get array [ascii|ieee754]");
        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("var %s not found", argv[0]);
        return 2;
    }

    if(var->type != ARRAY_FLT)
    {
    	uputs("not an array");
    	return 3;
    }

    /* format specified */
    if(argc == 2)
    {
    	if(strcmp(argv[1], "ascii") == 0)
    	{
    		format = ASCII;
    	}
    	else if(strcmp(argv[1], "ieee754") == 0)
    	{
    		format = IEEE754;
    	}
    	else
    	{
    		uputs("unknown format");
    		return 4;
    	}
    }

    /* print the array content */
    if(format == ASCII)
    {
        for(int i = 0; i < var->array_len; i++)
        {
        	uprintf("%g,", *((float *)var->ptr + i));
        }
    }
    else if(format == IEEE754)
    {
    	print_variable_record((float *)var->ptr, var->array_len);
    }
    else
    {
    	uputs("unknown format");
    }

    return 0;
}

COMMAND_HANDLER(var_get_prop)
{
    if(argc != 1)
    {
        uprintf("usage: var_get_prop var");

        return 1;
    }

    aliased_variable_t *var = (aliased_variable_t *)extract_variable(argv[0]);

    if(!var)
    {
        uprintf("var %s not found", argv[0]);
        return 2;
    }

    uputs(argv[0]);
    uputs(":");

    switch(var->type)
    {
        case INT32:
            uputs("int32");
            break;
        case FLOAT32:
            uputs("float32");
            break;
        case INT64:
            uputs("int64");
            break;
        case ANGULAR_POS:
        	uputs("angular_pos");
        	break;
        case ARRAY_FLT:
        	uputs("array_float");
        	break;

        default:
            uprintf("???");
    }

    uprintf(",%d\r\n", var->read_only);

    return 0;
}
