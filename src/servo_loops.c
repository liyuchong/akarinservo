/*
 * Copyright (c) 2015 - 2017 Yuchong Li <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "hw/sharc_hw.h"
#include "motor_control.h"
#include <inttypes.h>

#include "kalman/kalman_filter.h"
#include "command/command.h"

static inline
float calculate_cogging_torque(float pos, float *cog_torque, int len)
{
    float index_frac = pos / PERIOD_REVOLUTION * len;

    int index = index_frac;
    int index_prev = index - 1;

    float t = index_frac - index;

    /* wrap around */
    if(index_prev < 0)
    {
        index_prev = len - 1;
    }

    if(index > len - 1)
    {
        index = len - 1;
    }

    return cog_torque[index] * t + (1.0 - t) * cog_torque[index_prev];
}

static inline
float calculate_session_cogging_torque(motor_control_session_t *session)
{
    /* cogging torque compensation using static torque-position map */
    /* Cogging Torque Ripple Minimization via Position-Based Characterization
     * Matthew Piccoli and  Mark Yim */

	return calculate_cogging_torque
	(
		/* convert encoder reading to [0, 16384), mechanical angle */
	    (float)session->encoder_raw * session->encoder_info.encoder_ratio,
		session->encoder_configs.cogging_torque, ANTI_COGGING_MAP_LEN
	);
}

static inline
void motor_control_position_loop(motor_control_session_t *session, float sint, float cost, float angle_rad)
{
	session->performance.pos_loop_tick++;

    // TODO: use diff ab-observer
    /* observe the velocity of the position command */
    ab_observer_update(&session->vel_ff_observer, angular_pos_to_float(session->rot_pos_set, PERIOD_REVOLUTION));
    session->pos_set_d = session->vel_ff_observer.vk_1;

    // XXX: test
    //session->rot_pos_set = angular_pos_from_float(session->vel_ff_observer.xk_1, PERIOD_REVOLUTION);

    session->pos_error =
    		angular_pos_difference_float(
    				session->rot_pos_set, session->rot_pos_meas,
					PERIOD_REVOLUTION
	);

    session->pos_error = average_sampler_update(&session->position_sampler, session->pos_error, NULL);
	session->pos_error = iir_filter_nth_update(&session->vel_command_filter, session->pos_error) * 0.06745527388;

    /* calculate change of error */
    session->pos_derror = differential_float(
            &session->pos_error_diff, session->pos_error
    );


#if 0
    /* tune the controller based on error and change of error
     * using fuzzy logic rules */
    motor_control_self_tuning(
            &session->pos_pid_tuner,
            &session->position_controller,
            session->pos_error, session->pos_derror
    );
#endif

    /* calculate velocity feed-forward term and clamp it within a range */
    const float vel_feedforward_term = var_range_clamp(
             session->encoder_configs.vel_feedforward * session->pos_set_d,
            -session->encoder_configs.rotor_vel_limit, session->encoder_configs.rotor_vel_limit
    );

	/* -------------------------
	 * PID control loop
	 * POSITION LOOP
	 * -------------------------
	 *  */
	session->rot_vel_set =
		pid_update(
			&session->position_controller,
			session->pos_error, /* reference and measured term */
			session->pos_derror * session->encoder_info.deg_per_pulse,       /* change of error */
			CONTROL_LOOP_TICK_TIME,                  /* loop period */
		   -session->encoder_configs.rotor_vel_limit,        /* output saturation range */
			session->encoder_configs.rotor_vel_limit,

			/* integral saturation range */
		   -session->rot_ang_vel_integral_max, session->rot_ang_vel_integral_max,
			vel_feedforward_term                     /* feed forward term */
	);
}

static inline
void motor_control_vel_loop(motor_control_session_t *session, float sint, float cost, float angle_rad)
{
    session->performance.vel_loop_tick++;

    /* calculate velocity error */
    session->vel_error = session->rot_vel_set - session->rot_vel_meas;
    /* calculate change of velocity error */
    session->vel_derror = differential_float(&session->vel_error_diff, session->vel_error);

    /* calculate cogging torque compensation value */
    float torque_cogging_compensation_term =
#if ANTI_COGGING
    calculate_session_cogging_torque(session);
    session->torque_meas -= torque_cogging_compensation_term;
#else
    0.0f;
#endif

    float friction_torque_compensation_term =
    		linear_evaluate(&session->fric_comp, session->rot_vel_meas / 6.0f);
    		//0.0f;

    // TODO: use diff ab-observer
    /* observe the velocity of the position command */
    ab_observer_update(&session->torque_ff_observer, session->pos_set_d);
    session->pos_set_dd = session->torque_ff_observer.vk_1;

    /* calculate torque feed-forward term and clamp it within a range */
    float torque_feedforward_term = var_range_clamp
    (
    	/* torque feed-forward from d(vel)/dt */
        (session->encoder_configs.torque_feedforward * session->pos_set_dd) *
		session->load_inertia_ratio -
		torque_cogging_compensation_term +
		friction_torque_compensation_term,

		-session->encoder_configs.torque_limit_ccw, session->encoder_configs.torque_limit_cw
    );

#if VEL_LOOP_USE_PDFF

    session->torque_set = -pid_pdff_update(
    		&session->velocity_controller,
			 session->rot_vel_set,
			 session->rot_vel_meas,
			 CONTROL_LOOP_TICK_TIME,
			 -session->encoder_configs.torque_limit_ccw,
			 session->encoder_configs.torque_limit_ccw,
			 -50.0f,
			 50.0f,
			 torque_feedforward_term
    );
#else

    session->torque_set = -pi_update(
            &session->velocity_controller,
			session->vel_error * 0.001f,            	/* error term */
            CONTROL_LOOP_TICK_TIME,           			/* loop period */
           -session->encoder_configs.torque_limit_ccw,	/* output saturation range */
           session->encoder_configs.torque_limit_cw,
		   -session->encoder_configs.torque_limit_ccw,										/* integral saturation range */
		   session->encoder_configs.torque_limit_cw,
            torque_feedforward_term           			/* feed forward term */
    );

#endif /* VEL_LOOP_USE_PDFF */

    //session->rot_vel_set = iir_filter_nth_update(&session->vel_command_filter, session->rot_vel_set)* 0.005542717210;

    // XXX: filter test
    for(uint32_t i = 0; i < SESSION_IIR_FILTER_COUNT; i++)
    {
    	/* if filter enabled */
    	if(session->enabled_iir_notch_filters & (1 << i))
    	{
    		/* apply the filter */
    		session->rot_vel_set = iir_filter_update(&session->notch_filters[i], session->rot_vel_set);
    	}
    }
}

static inline
void motor_control_torque_loop(motor_control_session_t *session, float sint, float cost, float angle_rad)
{
    if(session->fft_session.notch_filter_enabled)
    {
    	session->rot_vel_set = iir_filter_update(&session->fft_session.notch_filter, session->rot_vel_set);
    }

//    /* calculate cogging torque compensation value */
//#if ANTI_COGGING
//    float torque_cogging_compensation_term =
//    calculate_session_cogging_torque(session);
//    session->torque_meas -= torque_cogging_compensation_term;
//    session->torque_set -= torque_cogging_compensation_term;
//#endif

    // --------
    // TODO: vibration suppression notch filter here
    // --------


    /* -------------------------
     * PID control loop
     * TORQUE LOOP
     * -------------------------
     *  */
    session->current_cmd = pi_update(
                    &session->torque_controller,
                    session->torque_set -         /* reference term */
                    session->torque_meas,         /* measured term */
                    CONTROL_LOOP_TICK_TIME,       /* loop period */
                   -0.99,                         /* output saturation range */
                    0.99,                         /* integral saturation range */
                   -0.95, 0.95,
				    0.0f                          /* feed forward term */
    );

    motor_control_modulate(session->flux_cmd, session->current_cmd, sint, cost);
}

void motor_control_servo_loop(motor_control_session_t *session, float sint, float cost, float angle_rad)
{
    /* pulse input handling */
    if(mc_session.pulse_move_amount != 0)
    {
    	int32_t move_amount = mc_session.pulse_move_amount;
    	mc_session.pulse_move_amount = 0;

    	mc_session.rot_pos_set = angular_pos_plus_pos(mc_session.rot_pos_set, (float)move_amount, PERIOD_REVOLUTION);
    }

    session->pos_set_d = 0.0f;

    session->led_tick_count++;
    session->tick++;

#if VIBS_FFT_ENABLED
    if(session->state == CONTROL_POSITION || session->state == CONTROL_MOTION ||
    		session->state == CONTROL_VELOCITY)
    {
        /* FFT sample & calculation for velocity error signal */
        motor_control_fft(session);
    }
#endif

#define RUN_LOOP_NONE	  0
#define RUN_LOOP_POSITION 1
#define RUN_LOOP_VELOCITY 2
#define RUN_LOOP_TORQUE   4

    int enabled_loops = RUN_LOOP_NONE;

    if(session->state == CONTROL_POSITION || session->state == CONTROL_MOTION)
    {
    	enabled_loops = RUN_LOOP_POSITION | RUN_LOOP_VELOCITY | RUN_LOOP_TORQUE;
    }
    else if(session->state == CONTROL_VELOCITY)
    {
    	session->rot_vel_set = session->rotor_angular_vel_control;
    	enabled_loops = RUN_LOOP_VELOCITY | RUN_LOOP_TORQUE;
    }
    else if(session->state == CONTROL_TORQUE)
    {
    	session->torque_set = session->free_run_torque;
    	enabled_loops = RUN_LOOP_TORQUE;
    }
    else if(session->state == CONTROL_SET_ANGLE)
    {
    	float ang = RAD(session->set_angle);
        motor_control_modulate(0, session->free_run_torque, cosf(ang), sinf(ang));
    }
    else if(session->state == CONTROL_FB)
    {
    	float fff = session->flux_cmd;

    	if(fff > 0.1) fff = 0.1;
    	if(fff < -0.1) fff = -0.1;

        motor_control_modulate(fff, session->free_run_torque, sint, cost);
    }
    else
    {
        /* CONTROL_PAUSED mode, disable all PWM outputs */
        update_pwm_comp(0, 0, 0);
    }

    /* run the selected servo loops */
    if(enabled_loops & RUN_LOOP_POSITION)
    {
        motion_control_execute(session);
        motor_control_position_loop(session, sint, cost, angle_rad);
    }

    if(enabled_loops & RUN_LOOP_VELOCITY)
    {
    	motor_control_vel_loop(session, sint, cost, angle_rad);
    }

    if(enabled_loops & RUN_LOOP_TORQUE)
    {
    	motor_control_torque_loop(session, sint, cost, angle_rad);
    }

    /* calculate displayed angular velocity and torque using average filters */
    session->ang_vel_display = average_sampler_update(&session->ang_vel_sampler, session->rot_vel_meas, NULL);
    session->torque_display = average_sampler_update(&session->torque_sampler, session->torque_meas, NULL);

    /* out-of-control detection */
    if(abs(session->pos_error) > session->controller_configs.out_of_control_thres)
    {
    	CONTROL_STOP(session);
    	SET_ERROR(ERROR_OUT_OF_CONTROL);
    }
    else
    {
    	CLR_ERROR(ERROR_OUT_OF_CONTROL);
    }

}
